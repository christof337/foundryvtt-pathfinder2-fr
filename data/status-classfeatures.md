# État de la traduction (classfeatures)

 * **aucune**: 3
 * **officielle**: 233
 * **libre**: 127
 * **changé**: 24


Dernière mise à jour: 2020-11-16 19:02 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[alchemical-01-1eWDNcWSlNoU0L67.htm](classfeatures/alchemical-01-1eWDNcWSlNoU0L67.htm)|Formula Book (Alchemist)|
|[bard-01-6FsusoMYxxjyIkVh.htm](classfeatures/bard-01-6FsusoMYxxjyIkVh.htm)|Spell Repertoire (Bard)|
|[sorcerer-01-gmnx7e1g08bppbqt.htm](classfeatures/sorcerer-01-gmnx7e1g08bppbqt.htm)|Sorcerer Spellcasting|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[alchemist-01-7JbiaZ8bxODM5mzS.htm](classfeatures/alchemist-01-7JbiaZ8bxODM5mzS.htm)|Bomber|Artificier|officielle|
|[alchemist-01-eNZnx4LISDNftbx2.htm](classfeatures/alchemist-01-eNZnx4LISDNftbx2.htm)|Chirugeon|Chirurgien|officielle|
|[alchemist-01-P9quO9XZi3OWFe1k.htm](classfeatures/alchemist-01-P9quO9XZi3OWFe1k.htm)|Toxicologist|Toxicologiste|libre|
|[alchemist-01-Pe0zmIqyTBc2Td0I.htm](classfeatures/alchemist-01-Pe0zmIqyTBc2Td0I.htm)|Advanced Alchemy|Alchimie avancée|officielle|
|[alchemist-01-sPtl05wwTpqFI0lL.htm](classfeatures/alchemist-01-sPtl05wwTpqFI0lL.htm)|Quick Alchemy|Alchimie rapide|officielle|
|[alchemist-01-tvdb1jkjl2bRZjSp.htm](classfeatures/alchemist-01-tvdb1jkjl2bRZjSp.htm)|Mutagenist|Mutagèniste|officielle|
|[alchemist-01-wySB9VHOW1v3TX1L.htm](classfeatures/alchemist-01-wySB9VHOW1v3TX1L.htm)|Infused Reagents|Réactifs imprégnés|officielle|
|[alchemist-05-6zo2PJGYoig7nFpR.htm](classfeatures/alchemist-05-6zo2PJGYoig7nFpR.htm)|Field Discovery (Toxicologist)|Découverte de domaine de recherche (Toxicologiste)|libre|
|[alchemist-05-8QAFgy9U8PxEa7Dw.htm](classfeatures/alchemist-05-8QAFgy9U8PxEa7Dw.htm)|Field Discovery (Bomber)|Découverte de domaine de recherche (Artificier)|officielle|
|[alchemist-05-qC0Iz6SlG2i9gv6g.htm](classfeatures/alchemist-05-qC0Iz6SlG2i9gv6g.htm)|Field Discovery (Chirurgeon)|Découverte de domaine de recherche (Chirurgien)|changé|
|[alchemist-05-V4Jt7eDnJBLv5bDj.htm](classfeatures/alchemist-05-V4Jt7eDnJBLv5bDj.htm)|Field Discovery (Mutagenist)|Découverte de domaine de recherche (Mutagèniste)|officielle|
|[alchemist-07-4ocPy4O0OCLY0XCM.htm](classfeatures/alchemist-07-4ocPy4O0OCLY0XCM.htm)|Alchemical Weapon Expertise|Expertise avec les armes alchimiques|officielle|
|[alchemist-07-DFQDtT1Van4fFEHi.htm](classfeatures/alchemist-07-DFQDtT1Van4fFEHi.htm)|Perpetual Infusions (Bomber)|Infusions perpétuelles (Artificier)|officielle|
|[alchemist-07-Dug1oaVYejLmYEFt.htm](classfeatures/alchemist-07-Dug1oaVYejLmYEFt.htm)|Perpetual Infusions (Mutagenist)|Infusions perpétuelles (Mutagèniste)|officielle|
|[alchemist-07-fzvIe6FwwCuIdnjX.htm](classfeatures/alchemist-07-fzvIe6FwwCuIdnjX.htm)|Perpetual Infusions (Chirurgeon)|Infusions perpétuelles (Chirurgien)|officielle|
|[alchemist-07-LlZ5R50z9j8jysZL.htm](classfeatures/alchemist-07-LlZ5R50z9j8jysZL.htm)|Perpetual Infusions (Toxicologist)|Infusions perpétuelles (Toxicologiste)|libre|
|[alchemist-07-V761X63x7CZ7pnUG.htm](classfeatures/alchemist-07-V761X63x7CZ7pnUG.htm)|Iron Will (Alchemist)|Volonté de fer (Alchimiste)|officielle|
|[alchemist-09-3e1PlMXmlSwKoc6d.htm](classfeatures/alchemist-09-3e1PlMXmlSwKoc6d.htm)|Alchemical Expertise|Expertise alchimique|officielle|
|[alchemist-09-76cwNLJEm4Yetnee.htm](classfeatures/alchemist-09-76cwNLJEm4Yetnee.htm)|Double Brew|Double préparation|officielle|
|[alchemist-09-B0yWXftQ4VZqpvAC.htm](classfeatures/alchemist-09-B0yWXftQ4VZqpvAC.htm)|Alertness (Alchemist)|Vigilance (Alchimiste)|officielle|
|[alchemist-11-8rEVg03QJ71ic3PP.htm](classfeatures/alchemist-11-8rEVg03QJ71ic3PP.htm)|Perpetual Potency (Bomber)|Efficacité perpétuelle (Artificier)|officielle|
|[alchemist-11-JOdbVu14phvdjhaY.htm](classfeatures/alchemist-11-JOdbVu14phvdjhaY.htm)|Perpetual Potency (Toxicologist)|Efficacité perpétuelle (Toxicologiste)|libre|
|[alchemist-11-llCLtEzMTQwkXZnQ.htm](classfeatures/alchemist-11-llCLtEzMTQwkXZnQ.htm)|Juggernaut (Alchemist)|Juggernaut (Alchimiste)|officielle|
|[alchemist-11-mZFqRLYOQEqKA8ri.htm](classfeatures/alchemist-11-mZFqRLYOQEqKA8ri.htm)|Perpetual Potency (Mutagenist)|Efficacité perpétuelle (Mutagèniste)|officielle|
|[alchemist-11-VS5vkqUQu4n7E28Y.htm](classfeatures/alchemist-11-VS5vkqUQu4n7E28Y.htm)|Perpetual Potency (Chirurgeon)|Efficacité perpétuelle (Chirurgien)|officielle|
|[alchemist-13-1BKdOJ0HNL6Eg3xw.htm](classfeatures/alchemist-13-1BKdOJ0HNL6Eg3xw.htm)|Greater Field Discovery (Mutagenist)|Découverte de domaine de recherche supérieure (Mutagèniste)|officielle|
|[alchemist-13-1orZOFZgAiSmm9RL.htm](classfeatures/alchemist-13-1orZOFZgAiSmm9RL.htm)|Weapon Specialization (Alchemist)|Spécialisation martiale (Alchimiste)|officielle|
|[alchemist-13-JJcaVijwRt9dsnac.htm](classfeatures/alchemist-13-JJcaVijwRt9dsnac.htm)|Greater Field Discovery (Chirurgeon)|Découverte de domaine de recherche supérieure (Chirurgien)|officielle|
|[alchemist-13-OKUowbhHDzJUo4xz.htm](classfeatures/alchemist-13-OKUowbhHDzJUo4xz.htm)|Light Armor Expertise (Alchemist)|Expertise avec les armures légères (Alchimiste)|officielle|
|[alchemist-13-RGs4uR3CAvgbtBAA.htm](classfeatures/alchemist-13-RGs4uR3CAvgbtBAA.htm)|Greater Field Discovery (Bomber)|Découverte de domaine de recherche supérieure (Artificier)|officielle|
|[alchemist-13-tnqyQrhrZeDtDvcO.htm](classfeatures/alchemist-13-tnqyQrhrZeDtDvcO.htm)|Greater Field Discovery (Toxicologist)|Découverte de domaine de recherche supérieure (Toxicologiste)|libre|
|[alchemist-15-Eood6pNPaJxuSgD1.htm](classfeatures/alchemist-15-Eood6pNPaJxuSgD1.htm)|Alchemical Alacrity|Alacrité alchimique|officielle|
|[alchemist-15-N9qTieoFl1nXXlsl.htm](classfeatures/alchemist-15-N9qTieoFl1nXXlsl.htm)|Evasion (Alchemist)|Évasion (Alchimiste)|officielle|
|[alchemist-17-3R19zS7gERhEX87F.htm](classfeatures/alchemist-17-3R19zS7gERhEX87F.htm)|Perpetual Perfection (Toxicologist)|Perfection perpétuelle (Toxicologiste)|libre|
|[alchemist-17-CGetAmSbv06fW7GT.htm](classfeatures/alchemist-17-CGetAmSbv06fW7GT.htm)|Perpetual Perfection (Mutagenist)|Perfection perpétuelle (Mutagèniste)|officielle|
|[alchemist-17-eG7FBDjCdEFzW9V9.htm](classfeatures/alchemist-17-eG7FBDjCdEFzW9V9.htm)|Alchemical Mastery|Maîtrise alchimique|officielle|
|[alchemist-17-xO90iBD8XNGyaCkz.htm](classfeatures/alchemist-17-xO90iBD8XNGyaCkz.htm)|Perpetual Perfection (Bomber)|Perfection perpétuelle (Artificier)|officielle|
|[alchemist-17-YByJ9O7oe8wxfbqs.htm](classfeatures/alchemist-17-YByJ9O7oe8wxfbqs.htm)|Perpetual Perfection (Chirurgeon)|Perfection perpétuelle (Chirurgien)|officielle|
|[alchemist-19-LOLVZkOvgWpzwsgq.htm](classfeatures/alchemist-19-LOLVZkOvgWpzwsgq.htm)|Light Armor Mastery (Alchemist)|Maîtrise des armures légères (Alchimiste)|officielle|
|[barbarian-01-0FtzFbUrN56KA67z.htm](classfeatures/barbarian-01-0FtzFbUrN56KA67z.htm)|Animal Instinct|Instinct animal|officielle|
|[barbarian-01-JuKD6k7nDwfO0Ckv.htm](classfeatures/barbarian-01-JuKD6k7nDwfO0Ckv.htm)|Giant Instinct|Instinct de géant|officielle|
|[barbarian-01-k7M9jedvt31AJ5ZR.htm](classfeatures/barbarian-01-k7M9jedvt31AJ5ZR.htm)|Fury Instinct|Instinct de la furie|officielle|
|[barbarian-01-SCYSjUbMmw8JD9P9.htm](classfeatures/barbarian-01-SCYSjUbMmw8JD9P9.htm)|Superstition Instinct|Instinct superstitieux|libre|
|[barbarian-01-TQqv9Q5mB4PW6LH9.htm](classfeatures/barbarian-01-TQqv9Q5mB4PW6LH9.htm)|Spirit Instinct|Instinct spirituel|officielle|
|[barbarian-01-VDot7CDcXElxmkkz.htm](classfeatures/barbarian-01-VDot7CDcXElxmkkz.htm)|Dragon Instinct|Instinct du dragon|officielle|
|[barbarian-01-WZUCvxqbigXos1L9.htm](classfeatures/barbarian-01-WZUCvxqbigXos1L9.htm)|Rage|Rage|officielle|
|[barbarian-03-ZW09VPBA4GhuYBbg.htm](classfeatures/barbarian-03-ZW09VPBA4GhuYBbg.htm)|Deny Advantage (Barbarian)|Refus d’avantage (Barbare)|officielle|
|[barbarian-05-EEUTd0jAyfwTLzjk.htm](classfeatures/barbarian-05-EEUTd0jAyfwTLzjk.htm)|Brutality|Brutalité|officielle|
|[barbarian-07-9EqIasqfI8YIM3Pt.htm](classfeatures/barbarian-07-9EqIasqfI8YIM3Pt.htm)|Weapon Specialization (Barbarian)|Spécialisation martiale (Barbare)|officielle|
|[barbarian-07-tItHGKsORVsUqC31.htm](classfeatures/barbarian-07-tItHGKsORVsUqC31.htm)|Juggernaut (Barbarian)|Juggernaut (Barbare)|officielle|
|[barbarian-09-ie6xDX9GMEcA2Iuq.htm](classfeatures/barbarian-09-ie6xDX9GMEcA2Iuq.htm)|Raging Resistance|Résistance enragée|officielle|
|[barbarian-09-Y84m6k4VsR66UZt4.htm](classfeatures/barbarian-09-Y84m6k4VsR66UZt4.htm)|Lightning Reflexes (Barbarian)|Réflexes éclair (Barbare)|officielle|
|[barbarian-11-88Q33X2a0iYPkbzd.htm](classfeatures/barbarian-11-88Q33X2a0iYPkbzd.htm)|Mighty Rage|Rage formidable|officielle|
|[barbarian-13-ejP4jVQkS48uKRFz.htm](classfeatures/barbarian-13-ejP4jVQkS48uKRFz.htm)|Weapon Fury|Fureur armée|officielle|
|[barbarian-13-TjHt63k8LykkANf2.htm](classfeatures/barbarian-13-TjHt63k8LykkANf2.htm)|Medium Armor Expertise (Barbarian)|Expertise avec les armures intermédiaires (Barbare)|officielle|
|[barbarian-13-TuL0UfqH14MtqYVh.htm](classfeatures/barbarian-13-TuL0UfqH14MtqYVh.htm)|Greater Juggernaut|Juggernaut supérieur|officielle|
|[barbarian-15-7JjhxMFo8DMwpGx0.htm](classfeatures/barbarian-15-7JjhxMFo8DMwpGx0.htm)|Greater Weapon Specialization (Barbarian)|Spécialisation martiale supérieure (Barbare)|officielle|
|[barbarian-15-BZnqKnqKVImjSIFE.htm](classfeatures/barbarian-15-BZnqKnqKVImjSIFE.htm)|Indomitable Will|Volonté indomptable|officielle|
|[barbarian-17-7MhzrbOyue5GQsck.htm](classfeatures/barbarian-17-7MhzrbOyue5GQsck.htm)|Heightened Senses|Sens aiguisés|officielle|
|[barbarian-17-qMtyQGUllPdgpzUo.htm](classfeatures/barbarian-17-qMtyQGUllPdgpzUo.htm)|Quick Rage|Rage rapide|officielle|
|[barbarian-19-QTCIahokREpnAYDi.htm](classfeatures/barbarian-19-QTCIahokREpnAYDi.htm)|Armor of Fury|Armure de furie|officielle|
|[barbarian-19-VLiT503OLOM3vaDx.htm](classfeatures/barbarian-19-VLiT503OLOM3vaDx.htm)|Devastator|Dévastateur|officielle|
|[bard-00-YMBsi4bndRAk5CX4.htm](classfeatures/bard-00-YMBsi4bndRAk5CX4.htm)|Maestro (Bard Muse)|Virtuose (Muse de barde)|officielle|
|[bard-01-4ripp6EfdVpS0d60.htm](classfeatures/bard-01-4ripp6EfdVpS0d60.htm)|Enigma (Bard Muse)|Énigmatique (Muse de barde)|officielle|
|[bard-01-fEOj0eOBe34qYdAa.htm](classfeatures/bard-01-fEOj0eOBe34qYdAa.htm)|Occult Spellcasting|Incantation occulte|officielle|
|[bard-01-N03BtRvjX9TeHRa4.htm](classfeatures/bard-01-N03BtRvjX9TeHRa4.htm)|Warrior (Bard Muse)|Combattant (Muse de barde)|libre|
|[bard-01-y0jGimYdMGDJWrEq.htm](classfeatures/bard-01-y0jGimYdMGDJWrEq.htm)|Polymath (Bard Muse)|Touche-à-tout|changé|
|[bard-03-nw4gH2jMQN6DRv8z.htm](classfeatures/bard-03-nw4gH2jMQN6DRv8z.htm)|Signature Spells (Bard)|Sorts emblématiques (Barde)|officielle|
|[bard-03-q00P563wwFZVYhyo.htm](classfeatures/bard-03-q00P563wwFZVYhyo.htm)|Lightning Reflexes (Bard)|Réflexes éclair (Barde)|officielle|
|[bard-07-6YIyycFN795wja1b.htm](classfeatures/bard-07-6YIyycFN795wja1b.htm)|Expert Spellcaster (Bard)|Incantateur expert (Barde)|officielle|
|[bard-09-AnZXGuP7Fa1mTiYJ.htm](classfeatures/bard-09-AnZXGuP7Fa1mTiYJ.htm)|Resolve (Bard)|Résolution (Barde)|officielle|
|[bard-09-YdH1LNS2mT3LBVHn.htm](classfeatures/bard-09-YdH1LNS2mT3LBVHn.htm)|Great Fortitude (Bard)|Vigueur surhumaine (Barde)|officielle|
|[bard-11-4lp8oG9A3zuqhPBS.htm](classfeatures/bard-11-4lp8oG9A3zuqhPBS.htm)|Bard Weapon Expertise|expertise avec les armes du barde|officielle|
|[bard-11-gNoqbqe18TWpppYe.htm](classfeatures/bard-11-gNoqbqe18TWpppYe.htm)|Vigilant Senses (Bard)|Sens alertes (Barde)|officielle|
|[bard-13-dtl8i1fYDNiumaay.htm](classfeatures/bard-13-dtl8i1fYDNiumaay.htm)|Light Armor Expertise (Bard)|Expertise avec les armures légères (Barde)|officielle|
|[bard-13-J5zCG0fMBFflfbSV.htm](classfeatures/bard-13-J5zCG0fMBFflfbSV.htm)|Weapon Specialization (Bard)|Spécialisation martiale (Barde)|officielle|
|[bard-15-5UPkTPy5u2fZKqUW.htm](classfeatures/bard-15-5UPkTPy5u2fZKqUW.htm)|Master Spellcaster (Bard)|Maître incantateur (Barde)|officielle|
|[bard-17-Km2CaLUuRaxn79kH.htm](classfeatures/bard-17-Km2CaLUuRaxn79kH.htm)|Greater Resolve (Bard)|Résolution accrue (Barde)|officielle|
|[bard-19-NjsOpWbbzUY2Hpk3.htm](classfeatures/bard-19-NjsOpWbbzUY2Hpk3.htm)|Magnum Opus|Magnum opus|officielle|
|[bard-19-tmO36DHYZo64iywr.htm](classfeatures/bard-19-tmO36DHYZo64iywr.htm)|Legendary Spellcaster (Bard)|Incantateur légendaire (Barde)|officielle|
|[champion-00-fykh5pE99O3I2sOI.htm](classfeatures/champion-00-fykh5pE99O3I2sOI.htm)|Champion Code|Code du champion|changé|
|[champion-01-8YIA0jh64Ecz0TG6.htm](classfeatures/champion-01-8YIA0jh64Ecz0TG6.htm)|Desecrator [Neutral Evil]|Profanateur [Neutre mauvais]|libre|
|[champion-01-ehL7mnkqxN5wIkgu.htm](classfeatures/champion-01-ehL7mnkqxN5wIkgu.htm)|Deity and Cause|Divinité et cause|officielle|
|[champion-01-EQ6DVIQHAUXUhY6Y.htm](classfeatures/champion-01-EQ6DVIQHAUXUhY6Y.htm)|Antipaladin [Chaotic Evil]|Antipaladin [Chaotique Mauvais]|libre|
|[champion-01-FCoMFUsth4xB4veC.htm](classfeatures/champion-01-FCoMFUsth4xB4veC.htm)|Liberator [Chaotic Good]|Libérateur (Chaotique Bon)|changé|
|[champion-01-FeBsYn2mHfMVDZvw.htm](classfeatures/champion-01-FeBsYn2mHfMVDZvw.htm)|Deific Weapon|Arme déifique|officielle|
|[champion-01-fVVRFfc8QwTTPhKf.htm](classfeatures/champion-01-fVVRFfc8QwTTPhKf.htm)|Shield Block (Champion)|Blocage au bouclier (Champion)|officielle|
|[champion-01-HiIvez0TqESbleB5.htm](classfeatures/champion-01-HiIvez0TqESbleB5.htm)|Tyrant [Lawful Evil]|Tyran [Loyal mauvais]|libre|
|[champion-01-JiY2ZB4FkK8RJm4T.htm](classfeatures/champion-01-JiY2ZB4FkK8RJm4T.htm)|The Tenets of Evil|Les préceptes du Mal|libre|
|[champion-01-nxZYP3KGfTSkaW6J.htm](classfeatures/champion-01-nxZYP3KGfTSkaW6J.htm)|The Tenets of Good|Les principes du bien|officielle|
|[champion-01-peEXunfbSD8WcMFk.htm](classfeatures/champion-01-peEXunfbSD8WcMFk.htm)|Paladin [Lawful Good]|Paladin (Loyal Bon)|changé|
|[champion-01-Q1VfQZp49hkhY0HY.htm](classfeatures/champion-01-Q1VfQZp49hkhY0HY.htm)|Devotion Spells|Sorts de dévotion|officielle|
|[champion-01-UyuwFp0jQqYL2AdF.htm](classfeatures/champion-01-UyuwFp0jQqYL2AdF.htm)|Redeemer [Neutral Good]|Rédempteur (Neutre Bon)|changé|
|[champion-03-ERwuazupczhUSZ73.htm](classfeatures/champion-03-ERwuazupczhUSZ73.htm)|Divine Ally|Allié divin|changé|
|[champion-05-fC8FbGmkTwKMU8FF.htm](classfeatures/champion-05-fC8FbGmkTwKMU8FF.htm)|Weapon Expertise (Champion)|Expertise avec les armes (Champion)|officielle|
|[champion-07-D5AABucMrk5IGOKV.htm](classfeatures/champion-07-D5AABucMrk5IGOKV.htm)|Weapon Specialization (Champion)|Spécialisation martiale (Champion)|officielle|
|[champion-07-g0nfwT61wE4vOaOn.htm](classfeatures/champion-07-g0nfwT61wE4vOaOn.htm)|Armor Expertise (Champion)|Expertise avec les armures|officielle|
|[champion-09-dAA1MElXsUjQ5fzw.htm](classfeatures/champion-09-dAA1MElXsUjQ5fzw.htm)|Juggernaut (Champion)|Juggernaut (Champion)|officielle|
|[champion-09-rl3b7Nv5N13UL9VC.htm](classfeatures/champion-09-rl3b7Nv5N13UL9VC.htm)|Lightning Reflexes (Champion)|Réflexes éclair (Champion)|officielle|
|[champion-09-VgmfNKtQLgBaNi5r.htm](classfeatures/champion-09-VgmfNKtQLgBaNi5r.htm)|Champion Expertise|Expertise du champion|officielle|
|[champion-11-agrDiX3aTSowtO9u.htm](classfeatures/champion-11-agrDiX3aTSowtO9u.htm)|Alertness (Champion)|Vigilance (Champion)|officielle|
|[champion-11-xygfZopqXBJ6dKBA.htm](classfeatures/champion-11-xygfZopqXBJ6dKBA.htm)|Divine Will|Résolution divine|officielle|
|[champion-13-Az2EoWOpDz4y9ajJ.htm](classfeatures/champion-13-Az2EoWOpDz4y9ajJ.htm)|Armor Mastery (Champion)|Maîtrise des armures (Champion)|officielle|
|[champion-13-gewtLQfU4D06D54G.htm](classfeatures/champion-13-gewtLQfU4D06D54G.htm)|Weapon Mastery (Champion)|Maîtrise martiale (Champion)|officielle|
|[champion-15-nRYGMlG5odMqsUJF.htm](classfeatures/champion-15-nRYGMlG5odMqsUJF.htm)|Greater Weapon Specialization (Champion)|Spécialisation martiale supérieure (Champion)|officielle|
|[champion-17-voiSCh7ZXA2ogwiC.htm](classfeatures/champion-17-voiSCh7ZXA2ogwiC.htm)|Legendary Armor|Armure légendaire|officielle|
|[champion-17-z5G0o04uV65zyxDB.htm](classfeatures/champion-17-z5G0o04uV65zyxDB.htm)|Champion Mastery|Maîtrise du Champion|officielle|
|[champion-19-LzB6X9vOaq3wq1FZ.htm](classfeatures/champion-19-LzB6X9vOaq3wq1FZ.htm)|Hero's Defiance|Défi du héros|officielle|
|[cleric-00-UV1HlClbWCNcaKBZ.htm](classfeatures/cleric-00-UV1HlClbWCNcaKBZ.htm)|Anathema (Cleric)|Anathème (Prêtre)|changé|
|[cleric-01-0Aocw3igLwna9cjp.htm](classfeatures/cleric-01-0Aocw3igLwna9cjp.htm)|Warpriest|Prêtre combattant|officielle|
|[cleric-01-AvNbdGSOTWNRgcxs.htm](classfeatures/cleric-01-AvNbdGSOTWNRgcxs.htm)|Divine Spellcasting (Cleric)|Incantation divine|changé|
|[cleric-01-gblTFUOgolqFS9v4.htm](classfeatures/cleric-01-gblTFUOgolqFS9v4.htm)|Divine Font|Source divine|officielle|
|[cleric-01-ZZzLMOUAtBVgV1DF.htm](classfeatures/cleric-01-ZZzLMOUAtBVgV1DF.htm)|Cloistered Cleric|Prêtre cloîtré|officielle|
|[cleric-05-EiD3tYJQAO4OhWc6.htm](classfeatures/cleric-05-EiD3tYJQAO4OhWc6.htm)|Alertness (Cleric)|Vigilance (Prêtre)|officielle|
|[cleric-09-Ye6ixydYMNzCZllo.htm](classfeatures/cleric-09-Ye6ixydYMNzCZllo.htm)|Resolve (Cleric)|Résolution (Champion)|officielle|
|[cleric-11-ffC40m5ebeeXkzhC.htm](classfeatures/cleric-11-ffC40m5ebeeXkzhC.htm)|Lightning Reflexes (Cleric)|Réflexes éclair (Prêtre)|officielle|
|[cleric-13-0mJTp4LdEHBLInoe.htm](classfeatures/cleric-13-0mJTp4LdEHBLInoe.htm)|Divine Defense|Défense divine|officielle|
|[cleric-13-tga0Gdns1uA6jirb.htm](classfeatures/cleric-13-tga0Gdns1uA6jirb.htm)|Weapon Specialization (Cleric)|Spécialisation martiale (Prêtre)|officielle|
|[cleric-19-3uf31A91h3ywmlqm.htm](classfeatures/cleric-19-3uf31A91h3ywmlqm.htm)|Miraculous Spell|Sort miraculeux|officielle|
|[druid-00-nfBn8QB6HVdzpTFV.htm](classfeatures/druid-00-nfBn8QB6HVdzpTFV.htm)|Anathema (Druid)|Anathème (Druide)|officielle|
|[druid-01-acqqlYmti8D9QJi0.htm](classfeatures/druid-01-acqqlYmti8D9QJi0.htm)|Storm|Tempête|officielle|
|[druid-01-b8pnRxGuNzG0buuh.htm](classfeatures/druid-01-b8pnRxGuNzG0buuh.htm)|Primal Spellcasting|Incantation primordiale|officielle|
|[druid-01-copeE8gaSFVSXRbk.htm](classfeatures/druid-01-copeE8gaSFVSXRbk.htm)|Shield Block (Druid)|Blocage au bouclier (Druide)|officielle|
|[druid-01-d5BFFHXFJYKs5LXr.htm](classfeatures/druid-01-d5BFFHXFJYKs5LXr.htm)|Wild Empathy|Empathie sauvage|officielle|
|[druid-01-POBvoXifa9HaejAg.htm](classfeatures/druid-01-POBvoXifa9HaejAg.htm)|Animal|Animal|officielle|
|[druid-01-RiAGlnnp4S21BAG3.htm](classfeatures/druid-01-RiAGlnnp4S21BAG3.htm)|Druidic Language|Le druidique|officielle|
|[druid-01-u4nlOzPj2WHkIj9l.htm](classfeatures/druid-01-u4nlOzPj2WHkIj9l.htm)|Leaf|Feuille|officielle|
|[druid-01-v0EjtiwdeMj8ykI0.htm](classfeatures/druid-01-v0EjtiwdeMj8ykI0.htm)|Wild|Féroce|officielle|
|[druid-03-C62VDef26Wi2s4Qm.htm](classfeatures/druid-03-C62VDef26Wi2s4Qm.htm)|Alertness (Druid)|Vigilance (Druide)|officielle|
|[druid-03-IsicKhmKtNyrkI97.htm](classfeatures/druid-03-IsicKhmKtNyrkI97.htm)|Great Fortitude (Druid)|Vigueur surhumaine (Druide)|officielle|
|[druid-05-71djVINXSPTHO4xX.htm](classfeatures/druid-05-71djVINXSPTHO4xX.htm)|Lightning Reflexes (Druid)|Réflexes éclair (Druide)|officielle|
|[druid-07-7uV3wuwrceEx3NGf.htm](classfeatures/druid-07-7uV3wuwrceEx3NGf.htm)|Expert Spellcaster (Druid)|Incantateur expert (Druide)|officielle|
|[druid-11-4wBmnZowB2uXrVv3.htm](classfeatures/druid-11-4wBmnZowB2uXrVv3.htm)|Resolve (Druid)|Résolution (Druide)|officielle|
|[druid-11-Ra32tlqBxHzT6fzN.htm](classfeatures/druid-11-Ra32tlqBxHzT6fzN.htm)|Druid Weapon Expertise|Expertise avec les armes de druide|officielle|
|[druid-13-38LFTCPYmVzjOb5t.htm](classfeatures/druid-13-38LFTCPYmVzjOb5t.htm)|Weapon Specialization (Druid)|Spécialisation martiale (Druide)|officielle|
|[druid-13-6uOx7W6I7u5Jv5Lg.htm](classfeatures/druid-13-6uOx7W6I7u5Jv5Lg.htm)|Medium Armor Expertise (Druid)|Expertise avec les armures intermédiaires (Druide)|officielle|
|[druid-15-nlOj4yFnTbw7oNDj.htm](classfeatures/druid-15-nlOj4yFnTbw7oNDj.htm)|Master Spellcaster (Druid)|Maître incantateur (Druide)|officielle|
|[druid-19-nzgb43mQmLgaqDoQ.htm](classfeatures/druid-19-nzgb43mQmLgaqDoQ.htm)|Primal Hierophant|Hiérophante primordial|officielle|
|[druid-19-OdnfSYitJ5FfXGYW.htm](classfeatures/druid-19-OdnfSYitJ5FfXGYW.htm)|Legendary Spellcaster (Druid)|Incantateur légendaire (Druide)|officielle|
|[fighter-01-hmShTfPOcTaKgbf4.htm](classfeatures/fighter-01-hmShTfPOcTaKgbf4.htm)|Attack of Opportunity|Attaque d’opportunité|officielle|
|[fighter-01-OPlu94V5ibX5pr9u.htm](classfeatures/fighter-01-OPlu94V5ibX5pr9u.htm)|Shield Block (Fighter)|Blocage au bouclier (Guerrier)|officielle|
|[fighter-03-GJKJafDGuX4BeAeN.htm](classfeatures/fighter-03-GJKJafDGuX4BeAeN.htm)|Bravery|Bravoure|officielle|
|[fighter-05-gApJtAdNb9ST4Ms9.htm](classfeatures/fighter-05-gApJtAdNb9ST4Ms9.htm)|Fighter Weapon Mastery|Maîtrise martiale du guerrier|officielle|
|[fighter-07-TIvzBALymvb56L79.htm](classfeatures/fighter-07-TIvzBALymvb56L79.htm)|Battlefield Surveyor|Sentinelle au front|officielle|
|[fighter-07-vmGFRT812VfKGWAL.htm](classfeatures/fighter-07-vmGFRT812VfKGWAL.htm)|Weapon Specialization (Fighter)|Spécialisation martiale (Guerrier)|officielle|
|[fighter-09-8g6HzARbhfcgilP8.htm](classfeatures/fighter-09-8g6HzARbhfcgilP8.htm)|Combat Flexibility|Flexibilité martiale|officielle|
|[fighter-09-MEWrviI3SbdphhRm.htm](classfeatures/fighter-09-MEWrviI3SbdphhRm.htm)|Juggernaut (Fighter)|Juggernaut (Guerrier)|officielle|
|[fighter-11-1AR2xYvG6SHLy3Pk.htm](classfeatures/fighter-11-1AR2xYvG6SHLy3Pk.htm)|Armor Expertise (Fighter)|Expertise avec les armures (Guerrier)|officielle|
|[fighter-11-bAaI7h937Nr3g93U.htm](classfeatures/fighter-11-bAaI7h937Nr3g93U.htm)|Fighter Expertise|Expertise du Guerrier|officielle|
|[fighter-13-F5VenhIQMDkeGvmV.htm](classfeatures/fighter-13-F5VenhIQMDkeGvmV.htm)|Weapon Legend|Légende martiale|officielle|
|[fighter-15-k78c939FtT6zqLOZ.htm](classfeatures/fighter-15-k78c939FtT6zqLOZ.htm)|Greater Weapon Specialization (Fighter)|Légende martiale supérieure (Guerrier)|officielle|
|[fighter-15-kay3GfPFknMkSsU0.htm](classfeatures/fighter-15-kay3GfPFknMkSsU0.htm)|Evasion (Fighter)|Évasion (Guerrier)|officielle|
|[fighter-15-W2rwudMNcAxs8VoX.htm](classfeatures/fighter-15-W2rwudMNcAxs8VoX.htm)|Improved Flexibility|Flexibilité améliorée|officielle|
|[fighter-17-Ix6Y5Ne24mysyWts.htm](classfeatures/fighter-17-Ix6Y5Ne24mysyWts.htm)|Armor Mastery (Fighter)|Maîtrise des armures|officielle|
|[fighter-19-0H2LxtiZTJ275pSD.htm](classfeatures/fighter-19-0H2LxtiZTJ275pSD.htm)|Versatile Legend|Légende polyvalente|officielle|
|[investigator-01-2Fe4YZCvAr9Yf6w7.htm](classfeatures/investigator-01-2Fe4YZCvAr9Yf6w7.htm)|Strategic Strike|Frappe stratégique|libre|
|[investigator-01-6FasgIXUJ1X8ekRn.htm](classfeatures/investigator-01-6FasgIXUJ1X8ekRn.htm)|On the Case|Sur l'affaire|libre|
|[investigator-01-g3mNzNphtVxyR9Xr.htm](classfeatures/investigator-01-g3mNzNphtVxyR9Xr.htm)|Empiricism Methodology|Méthodologie : empirisme|changé|
|[investigator-01-lgo65ldX7WkXC8Ir.htm](classfeatures/investigator-01-lgo65ldX7WkXC8Ir.htm)|Devise a Strategem|Concevoir un stratagème|libre|
|[investigator-01-ln2Y1a4SxlU9sizX.htm](classfeatures/investigator-01-ln2Y1a4SxlU9sizX.htm)|Alchemical Sciences Methodology|Méthodologie : Sciences alchimiques|libre|
|[investigator-01-O3IX7rTxXWWvDVM3.htm](classfeatures/investigator-01-O3IX7rTxXWWvDVM3.htm)|Forensic Medicine Methodology|Méthodologie : Médecine forensique|libre|
|[investigator-01-UIHUNNYZyQ3p4Vmo.htm](classfeatures/investigator-01-UIHUNNYZyQ3p4Vmo.htm)|Interrogation Methodology|Méthodologie : Interrogatoire|libre|
|[investigator-03-dmK1wya8GBi9MmCB.htm](classfeatures/investigator-03-dmK1wya8GBi9MmCB.htm)|Skillful Lessons|Leçons accomplies|libre|
|[investigator-03-DZWQspPi4IkfXV2E.htm](classfeatures/investigator-03-DZWQspPi4IkfXV2E.htm)|Keen Recollection|Souvenirs affûtés|libre|
|[investigator-05-zmgJoO03w6MFoHWm.htm](classfeatures/investigator-05-zmgJoO03w6MFoHWm.htm)|Weapon Expertise (Investigator)|Expertise martiale (Enquêteur)|libre|
|[investigator-07-K1R079i5x3Aw4qs8.htm](classfeatures/investigator-07-K1R079i5x3Aw4qs8.htm)|Weapon Specialization (Investigator)|Spécialisation martiale (Enquêteur)|libre|
|[investigator-07-ZZ85PGb2REtS1qFg.htm](classfeatures/investigator-07-ZZ85PGb2REtS1qFg.htm)|Vigilant Senses (Investigator)|Sens alertes (Enquêteur)|libre|
|[investigator-09-PFvB79O2VFdiAeSj.htm](classfeatures/investigator-09-PFvB79O2VFdiAeSj.htm)|Investigator Expertise|Expertise de l'enquêteur|libre|
|[investigator-09-SHZXbRSq8e4gwsEl.htm](classfeatures/investigator-09-SHZXbRSq8e4gwsEl.htm)|Great Fortitude (Investigator)|Vigueur surhumaine (Enquêteur)|libre|
|[investigator-11-eFIfzdXf7uh9qtos.htm](classfeatures/investigator-11-eFIfzdXf7uh9qtos.htm)|Resolve (Investigator)|Résolution (Enquêteur)|libre|
|[investigator-11-malYpr0CYL4fDGhr.htm](classfeatures/investigator-11-malYpr0CYL4fDGhr.htm)|Deductive Improvisation|Improvisation déductive|libre|
|[investigator-13-fFV3dMR9wFWH0syt.htm](classfeatures/investigator-13-fFV3dMR9wFWH0syt.htm)|Incredible Senses (Investigator)|Sens extraordinaires (Enquêteur)|libre|
|[investigator-13-m6XdXqZ1lh3UcK2v.htm](classfeatures/investigator-13-m6XdXqZ1lh3UcK2v.htm)|Light Armor Expertise (Investigator)|Expertise avec les armures légères (Enquêteur)|libre|
|[investigator-13-PTYcCEMW5LvMLzxY.htm](classfeatures/investigator-13-PTYcCEMW5LvMLzxY.htm)|Weapon Mastery (Investigator)|Maîtrise martiale (Enquêteur)|libre|
|[investigator-15-lJdo5cFn2Hr0G3bc.htm](classfeatures/investigator-15-lJdo5cFn2Hr0G3bc.htm)|Evasion (Investigator)|Évasion (Enquêteur)|libre|
|[investigator-15-zNH4fa2sHkiqUNnm.htm](classfeatures/investigator-15-zNH4fa2sHkiqUNnm.htm)|Greater Weapon Specialization (Investigator)|Spécialisation martiale supérieure (Enquêteur)|libre|
|[investigator-17-F5AhrHtGf1mociZ0.htm](classfeatures/investigator-17-F5AhrHtGf1mociZ0.htm)|Greater Resolve (Investigator)|Résolution accrue (Enquêteur)|libre|
|[investigator-19-flEx8eY0NinF9XZU.htm](classfeatures/investigator-19-flEx8eY0NinF9XZU.htm)|Master Detective|Maître détective|libre|
|[investigator-19-uWjVimQPVYiPViD3.htm](classfeatures/investigator-19-uWjVimQPVYiPViD3.htm)|Light Armor Mastery (Investigator)|Maîtrise des armures légères (Enquêteur)|libre|
|[monk-01-NLHHHiAcdnZ5ohc2.htm](classfeatures/monk-01-NLHHHiAcdnZ5ohc2.htm)|Flurry of Blows|Déluge de coups|officielle|
|[monk-01-SB8UJ8rZmvbcBweJ.htm](classfeatures/monk-01-SB8UJ8rZmvbcBweJ.htm)|Powerful Fist|Poings puissants|officielle|
|[monk-03-Cq6NjvcKZOMySBVj.htm](classfeatures/monk-03-Cq6NjvcKZOMySBVj.htm)|Incredible Movement|Déplacement extraordinaire|officielle|
|[monk-03-D2AE8RfMlZ3D1FuV.htm](classfeatures/monk-03-D2AE8RfMlZ3D1FuV.htm)|Mystic Strikes|Frappes mystiques|officielle|
|[monk-05-7NCp6uuevOsaYRQB.htm](classfeatures/monk-05-7NCp6uuevOsaYRQB.htm)|Alertness (Monk)|Vigilance (Moine)|officielle|
|[monk-05-VgZIutWjFl8oZQFi.htm](classfeatures/monk-05-VgZIutWjFl8oZQFi.htm)|Expert Strikes|Frappes expertes|officielle|
|[monk-07-CfxBHF09tU4kDo0y.htm](classfeatures/monk-07-CfxBHF09tU4kDo0y.htm)|Path to Perfection|Voie vers la perfection|officielle|
|[monk-07-vV1Ko7dhcHyAGrId.htm](classfeatures/monk-07-vV1Ko7dhcHyAGrId.htm)|Weapon Specialization (Monk)|Spécialisation martiale (Moine)|officielle|
|[monk-09-CoRfFkisEsHE1e43.htm](classfeatures/monk-09-CoRfFkisEsHE1e43.htm)|Metal Strikes|Frappes de métal|officielle|
|[monk-09-lxImO5D0qWp0gXFB.htm](classfeatures/monk-09-lxImO5D0qWp0gXFB.htm)|Monk Expertise|Expertise du moine|officielle|
|[monk-11-0iCVX6h2RwC4mOGS.htm](classfeatures/monk-11-0iCVX6h2RwC4mOGS.htm)|Second Path to Perfection|Deuxième voie vers la Perfection|officielle|
|[monk-13-0iidKkzC2yy13lIf.htm](classfeatures/monk-13-0iidKkzC2yy13lIf.htm)|Master Strikes|Frappes de maître|officielle|
|[monk-13-95LI24ZSx0d4qfKX.htm](classfeatures/monk-13-95LI24ZSx0d4qfKX.htm)|Graceful Mastery|Maîtrise gracieuse|officielle|
|[monk-15-1m401NddgIh9llnJ.htm](classfeatures/monk-15-1m401NddgIh9llnJ.htm)|Third Path to Perfection|Troisème voie vers la Perfection|officielle|
|[monk-15-KKTfV9jhuxJuI6Qz.htm](classfeatures/monk-15-KKTfV9jhuxJuI6Qz.htm)|Greater Weapon Specialization (Monk)|Spécialisation martiale supérieure (Moine)|officielle|
|[monk-17-5cthRUkRqRtduVvN.htm](classfeatures/monk-17-5cthRUkRqRtduVvN.htm)|Adamantine Strikes|Frappes d'adamantium|officielle|
|[monk-17-JWDfzYub3JfuEtth.htm](classfeatures/monk-17-JWDfzYub3JfuEtth.htm)|Graceful Legend|Légende gracieuse|officielle|
|[monk-19-KmTfg7Sg5va4yU00.htm](classfeatures/monk-19-KmTfg7Sg5va4yU00.htm)|Perfected Form|Forme parfaite|officielle|
|[None-00-1FPVkksuE2ncw9rF.htm](classfeatures/None-00-1FPVkksuE2ncw9rF.htm)|Ki Spells|Sorts de ki|officielle|
|[None-00-s0VbbQJNlSgPocui.htm](classfeatures/None-00-s0VbbQJNlSgPocui.htm)|Composition Spells|Sorts de composition|officielle|
|[None-00-T25ZLQWn6O4KchLo.htm](classfeatures/None-00-T25ZLQWn6O4KchLo.htm)|Focus Spells|Sorts focalisés|libre|
|[oracle-01-7AVspOB6ITNzGFZi.htm](classfeatures/oracle-01-7AVspOB6ITNzGFZi.htm)|Divine Spellcasting (Oracle)|Incantation divine (Oracle)|libre|
|[oracle-01-cFe6vFb3gSDyNeS9.htm](classfeatures/oracle-01-cFe6vFb3gSDyNeS9.htm)|Spell Repertoire (Oracle)|Répertoire de sorts (Oracle)|changé|
|[oracle-01-gjOGOR30Czpnx3tM.htm](classfeatures/oracle-01-gjOGOR30Czpnx3tM.htm)|Battle Mystery|Mystère du combat|libre|
|[oracle-01-GTSvbFb36InvuH0w.htm](classfeatures/oracle-01-GTSvbFb36InvuH0w.htm)|Flames Mystery|Mystère des flammes|libre|
|[oracle-01-IaxmCkdsPlA52spu.htm](classfeatures/oracle-01-IaxmCkdsPlA52spu.htm)|Bones Mystery|Mystère des ossements|libre|
|[oracle-01-ibX2EhKkyUtbOHLj.htm](classfeatures/oracle-01-ibX2EhKkyUtbOHLj.htm)|Oracular Curse|Malédiction oraculaire|libre|
|[oracle-01-NXUOtO9NytHQurlg.htm](classfeatures/oracle-01-NXUOtO9NytHQurlg.htm)|Revelation Spells|Sorts de révélation|libre|
|[oracle-01-o1gGG36wpn9mxeop.htm](classfeatures/oracle-01-o1gGG36wpn9mxeop.htm)|Life Mystery|Mystère de la vie|libre|
|[oracle-01-PRJYLksQEwT39bTl.htm](classfeatures/oracle-01-PRJYLksQEwT39bTl.htm)|Mystery|Mystère|libre|
|[oracle-01-qvRlih3u7vK3FYUR.htm](classfeatures/oracle-01-qvRlih3u7vK3FYUR.htm)|Ancestors Mystery|Mystère des ancètres|libre|
|[oracle-01-RI2EMRBBPNSoTJXu.htm](classfeatures/oracle-01-RI2EMRBBPNSoTJXu.htm)|Cosmos Mystery|Mystère du cosmos|libre|
|[oracle-01-tZBb3Kh4nJcNoUFI.htm](classfeatures/oracle-01-tZBb3Kh4nJcNoUFI.htm)|Lore Mystery|Mystère du savoir|libre|
|[oracle-01-W9cF7wZztLDb1WGY.htm](classfeatures/oracle-01-W9cF7wZztLDb1WGY.htm)|Tempest Mystery|Mystère de la tempête|libre|
|[oracle-03-yzLt4zF1AIFi1u6r.htm](classfeatures/oracle-03-yzLt4zF1AIFi1u6r.htm)|Signature Spells (Oracle)|Sorts emblématiques (Oracle)|libre|
|[oracle-07-NYQ5ZKeCYky0wZyS.htm](classfeatures/oracle-07-NYQ5ZKeCYky0wZyS.htm)|Expert Spellcaster (Oracle)|Incantateur expert (Oracle)|libre|
|[oracle-07-WSzlr8UN2JGZ6LPC.htm](classfeatures/oracle-07-WSzlr8UN2JGZ6LPC.htm)|Resolve (Oracle)|Résolution (Oracle)|libre|
|[oracle-09-jugxK1UFi42N43iQ.htm](classfeatures/oracle-09-jugxK1UFi42N43iQ.htm)|Magical Fortitude (Oracle)|Vigueur magique (Oracle)|libre|
|[oracle-11-EmazNAghbeW9LxMG.htm](classfeatures/oracle-11-EmazNAghbeW9LxMG.htm)|Alertness (Oracle)|Vigilance (Oracle)|libre|
|[oracle-11-hLdPJs8OJ6ICnzys.htm](classfeatures/oracle-11-hLdPJs8OJ6ICnzys.htm)|Weapon Expertise (Oracle)|Expertise martiale (Oracle)|libre|
|[oracle-11-rrzItB68Er0DzKx7.htm](classfeatures/oracle-11-rrzItB68Er0DzKx7.htm)|Major Curse|Malédiction majeure|libre|
|[oracle-13-4a8T3KTCwIQ3kFCM.htm](classfeatures/oracle-13-4a8T3KTCwIQ3kFCM.htm)|Weapon Specialization (Oracle)|Spécialisation martiale (Oracle)|libre|
|[oracle-13-eluiQA9hfnJi0kPW.htm](classfeatures/oracle-13-eluiQA9hfnJi0kPW.htm)|Light Armor Expertise (Oracle)|Expertise avec les armures légères (Oracle)|libre|
|[oracle-13-leddTV9aD5lzYATy.htm](classfeatures/oracle-13-leddTV9aD5lzYATy.htm)|Lightning Reflexes (Oracle)|Réflexes éclair (Oracle)|libre|
|[oracle-15-u7gvhSh6OBIQPpOb.htm](classfeatures/oracle-15-u7gvhSh6OBIQPpOb.htm)|Master Spellcaster (Oracle)|Maître incantateur (Oracle)|libre|
|[oracle-17-F4brPlp1tHGUqyuI.htm](classfeatures/oracle-17-F4brPlp1tHGUqyuI.htm)|Extreme Curse|Malédiction extrême|libre|
|[oracle-17-fAod97Mp6ZJTwG0c.htm](classfeatures/oracle-17-fAod97Mp6ZJTwG0c.htm)|Greater Resolve (Oracle)|Résolution accrue (Oracle)|libre|
|[oracle-19-571c1aGnvNVwfF6b.htm](classfeatures/oracle-19-571c1aGnvNVwfF6b.htm)|Oracular Clarity|Clarté oraculaire|libre|
|[oracle-19-VAvEZ1zv8l45X9CE.htm](classfeatures/oracle-19-VAvEZ1zv8l45X9CE.htm)|Legendary Spellcaster (Oracle)|Incantateur légendaire (Oracle)|libre|
|[ranger-01-0nIOGpHQNHsKSFKT.htm](classfeatures/ranger-01-0nIOGpHQNHsKSFKT.htm)|Hunt Prey|Chasser une proie|officielle|
|[ranger-01-6v4Rj7wWfOH1882r.htm](classfeatures/ranger-01-6v4Rj7wWfOH1882r.htm)|Hunter's Edge: Flurry|spécialité du chasseur : Déluge|officielle|
|[ranger-01-NBHyoTrI8q62uDsU.htm](classfeatures/ranger-01-NBHyoTrI8q62uDsU.htm)|Hunter's Edge: Outwit|Spécialité du chasseur : Ruse|officielle|
|[ranger-01-u6cBjqz2fiRBadBt.htm](classfeatures/ranger-01-u6cBjqz2fiRBadBt.htm)|Hunter's Edge: Precision|Spécialité du chasseur : Précision|officielle|
|[ranger-03-hUl69qjTUBe0uGMA.htm](classfeatures/ranger-03-hUl69qjTUBe0uGMA.htm)|Iron Will (Ranger)|Volonté de fer (Rôdeur)|officielle|
|[ranger-05-PeZi7E9lI4vz8EGY.htm](classfeatures/ranger-05-PeZi7E9lI4vz8EGY.htm)|Trackless Step|Absence de traces|officielle|
|[ranger-05-QhoW8ivPvYmWzyEZ.htm](classfeatures/ranger-05-QhoW8ivPvYmWzyEZ.htm)|Ranger Weapon Expertise|Expertise avec les armes du rôdeur|officielle|
|[ranger-07-oIGvETC4fuunhMIH.htm](classfeatures/ranger-07-oIGvETC4fuunhMIH.htm)|Evasion (Ranger)|Évasion (Rôdeur)|officielle|
|[ranger-07-PnGcwyEiYdaCp8B9.htm](classfeatures/ranger-07-PnGcwyEiYdaCp8B9.htm)|Weapon Specialization (Ranger)|Spécialisation martiale (Rôdeur)|officielle|
|[ranger-07-Y7aYlSls1RWOre1c.htm](classfeatures/ranger-07-Y7aYlSls1RWOre1c.htm)|Vigilant Senses (Ranger)|Sens alertes (Rôdeur)|officielle|
|[ranger-09-5likl5SAxQPrQ3KF.htm](classfeatures/ranger-09-5likl5SAxQPrQ3KF.htm)|Ranger Expertise|Expertise du rôdeur|officielle|
|[ranger-09-j2R64kwUgEJ1TudD.htm](classfeatures/ranger-09-j2R64kwUgEJ1TudD.htm)|Nature's Edge|Avantage naturel|officielle|
|[ranger-11-18eLwCcdjwikfGsx.htm](classfeatures/ranger-11-18eLwCcdjwikfGsx.htm)|Juggernaut (Ranger)|Juggernaut (Rôdeur)|officielle|
|[ranger-11-cyGbjGdWLZOp4lCp.htm](classfeatures/ranger-11-cyGbjGdWLZOp4lCp.htm)|Medium Armor Expertise (Ranger)|Expertise avec les armures intermédiaires (Rôdeur)|officielle|
|[ranger-11-RlwE99yKnhq8FUuy.htm](classfeatures/ranger-11-RlwE99yKnhq8FUuy.htm)|Wild Stride|Déplacement facilité en milieu naturel|officielle|
|[ranger-13-oeGZz2WeFsAghvZ8.htm](classfeatures/ranger-13-oeGZz2WeFsAghvZ8.htm)|Weapon Mastery (Ranger)|Maîtrise martiale (Rôdeur)|officielle|
|[ranger-15-JzZn4IwIzHMntuIE.htm](classfeatures/ranger-15-JzZn4IwIzHMntuIE.htm)|Improved Evasion (Ranger)|Évasion améliorée (Rôdeur)|officielle|
|[ranger-15-lYmeakwbBjqKyLHI.htm](classfeatures/ranger-15-lYmeakwbBjqKyLHI.htm)|Incredible Senses (Ranger)|Sens extraordinaires (Rôdeur)|officielle|
|[ranger-15-ytP1RDTkFesbCLYB.htm](classfeatures/ranger-15-ytP1RDTkFesbCLYB.htm)|Greater Weapon Specialization (Ranger)|Spécialisation martiale supérieure (Rôdeur)|officielle|
|[ranger-17-RVZC4wVy5B5W2OeS.htm](classfeatures/ranger-17-RVZC4wVy5B5W2OeS.htm)|Masterful Hunter|Maître chasseur|changé|
|[ranger-19-bBGb1LcffXEqar0p.htm](classfeatures/ranger-19-bBGb1LcffXEqar0p.htm)|Swift Prey|Changement rapide de proie|officielle|
|[ranger-19-phwQ2MrDZ13D2HxC.htm](classfeatures/ranger-19-phwQ2MrDZ13D2HxC.htm)|Second Skin|Seconde peau|officielle|
|[rogue-01-3KPZ7svIO6kmmEKH.htm](classfeatures/rogue-01-3KPZ7svIO6kmmEKH.htm)|Ruffian Racket|Trafic de roublard : Voyou|changé|
|[rogue-01-D8qtAo2w4jsqjBrM.htm](classfeatures/rogue-01-D8qtAo2w4jsqjBrM.htm)|Eldritch Trickster Racket|Trafic de roublard : Mystificateur|libre|
|[rogue-01-j1JE61quDxdge4mg.htm](classfeatures/rogue-01-j1JE61quDxdge4mg.htm)|Sneak Attack|Attaque sournoise|officielle|
|[rogue-01-RyOkmu0W9svavuAB.htm](classfeatures/rogue-01-RyOkmu0W9svavuAB.htm)|Mastermind Racket|Trafic de roublard : Cerveau|libre|
|[rogue-01-uGuCGQvUmioFV2Bd.htm](classfeatures/rogue-01-uGuCGQvUmioFV2Bd.htm)|Rogue's Racket|Trafics de roublard|libre|
|[rogue-01-w6rMqmGzhUahdnA7.htm](classfeatures/rogue-01-w6rMqmGzhUahdnA7.htm)|Surprise Attack|Attaque surprise|officielle|
|[rogue-01-wAh2riuFRzz0edPl.htm](classfeatures/rogue-01-wAh2riuFRzz0edPl.htm)|Thief Racket|Trafic de roublard : Voleur|officielle|
|[rogue-01-ZvfxtUMtfIOLYHyg.htm](classfeatures/rogue-01-ZvfxtUMtfIOLYHyg.htm)|Scoundrel Racket|Trafic de roublard : Scélérat|officielle|
|[rogue-03-5AB9zIVADQvDG76w.htm](classfeatures/rogue-03-5AB9zIVADQvDG76w.htm)|Deny Advantage (Rogue)|Refus d'avantage (Roublard)|officielle|
|[rogue-05-v8UNEJR5IDKi8yqa.htm](classfeatures/rogue-05-v8UNEJR5IDKi8yqa.htm)|Weapon Tricks|Astuces martiales|officielle|
|[rogue-07-CeELxQ9E4dAyHmCX.htm](classfeatures/rogue-07-CeELxQ9E4dAyHmCX.htm)|Evasion (Rogue)|Évasion (Roublard)|officielle|
|[rogue-07-u6OsJxNFVpEs9aDK.htm](classfeatures/rogue-07-u6OsJxNFVpEs9aDK.htm)|Weapon Specialization (Rogue)|Spécialisation martiale (Roublard)|officielle|
|[rogue-07-ujJcCezhvLkbaOvb.htm](classfeatures/rogue-07-ujJcCezhvLkbaOvb.htm)|Vigilant Senses (Rogue)|Sens alertes (Roublard)|officielle|
|[rogue-09-9SruVg2lZpNaYLOB.htm](classfeatures/rogue-09-9SruVg2lZpNaYLOB.htm)|Debilitating Strikes|Frappes incapacitantes|officielle|
|[rogue-09-YfiiuhyXONCya73i.htm](classfeatures/rogue-09-YfiiuhyXONCya73i.htm)|Great Fortitude (Rogue)|Vigueur surhumaine (Roublard)|officielle|
|[rogue-11-f3Dh32EU4VsHu01b.htm](classfeatures/rogue-11-f3Dh32EU4VsHu01b.htm)|Rogue Expertise|Expertise du Roublard|officielle|
|[rogue-13-7Um2aWTbHDTOd7aq.htm](classfeatures/rogue-13-7Um2aWTbHDTOd7aq.htm)|Improved Evasion (Rogue)|Évasion améliorée (Roublard)|officielle|
|[rogue-13-LzNbksQx5DeDJESv.htm](classfeatures/rogue-13-LzNbksQx5DeDJESv.htm)|Light Armor Expertise (Rogue)|Expertise avec les armures légères (Roublard)|officielle|
|[rogue-13-myvcir1LEkaVxOlE.htm](classfeatures/rogue-13-myvcir1LEkaVxOlE.htm)|Master Tricks|Astuces de maître|officielle|
|[rogue-13-OqKMPnEbCKgvQgZM.htm](classfeatures/rogue-13-OqKMPnEbCKgvQgZM.htm)|Incredible Senses (Rogue)|Sens extraordinaires (Roublard)|officielle|
|[rogue-15-qQyKAktcAyfaiUm5.htm](classfeatures/rogue-15-qQyKAktcAyfaiUm5.htm)|Greater Weapon Specialization (Rogue)|Spécialisation martiale supérieure (Roublard)|officielle|
|[rogue-15-W1FkMHYVDg3yTU5r.htm](classfeatures/rogue-15-W1FkMHYVDg3yTU5r.htm)|Double Debilitation|Double handicap|officielle|
|[rogue-17-xmZ7oeTDcQVXegUP.htm](classfeatures/rogue-17-xmZ7oeTDcQVXegUP.htm)|Slippery Mind|Esprit fuyant|officielle|
|[rogue-19-51nIcXq8qPefb9vm.htm](classfeatures/rogue-19-51nIcXq8qPefb9vm.htm)|Light Armor Mastery (Rogue)|Maîtrise des armures légères (Roublard)|officielle|
|[rogue-19-SUUdWG0t33VKa5q4.htm](classfeatures/rogue-19-SUUdWG0t33VKa5q4.htm)|Master Strike|Frappe de maître|officielle|
|[sorcerer-01-2goYo6VNbwC6aKF1.htm](classfeatures/sorcerer-01-2goYo6VNbwC6aKF1.htm)|Bloodline|Lignages|libre|
|[sorcerer-01-3qqvnC2U8W26yae7.htm](classfeatures/sorcerer-01-3qqvnC2U8W26yae7.htm)|Bloodline: Aberrant|Lignage : Aberrant|changé|
|[sorcerer-01-5Wxjghw7lHuCxjZz.htm](classfeatures/sorcerer-01-5Wxjghw7lHuCxjZz.htm)|Bloodline: Nymph|Lignage : Nymphe|libre|
|[sorcerer-01-7WBZ2kkhZ7JorWu2.htm](classfeatures/sorcerer-01-7WBZ2kkhZ7JorWu2.htm)|Bloodline: Undead|Lignage : Mort-vivant|changé|
|[sorcerer-01-eW3cfCH7Wpx2vik2.htm](classfeatures/sorcerer-01-eW3cfCH7Wpx2vik2.htm)|Bloodline: Fey|Lignage : Féerique|changé|
|[sorcerer-01-lURKSJZAGKVD6cH9.htm](classfeatures/sorcerer-01-lURKSJZAGKVD6cH9.htm)|Spell Repertoire (Sorcerer)|Répertoire de sorts (Ensorceleur)|officielle|
|[sorcerer-01-O0uXZRWMNliDbkxU.htm](classfeatures/sorcerer-01-O0uXZRWMNliDbkxU.htm)|Bloodline: Hag|Lignage : Guenaude|changé|
|[sorcerer-01-o39zQMIdERWtmBSB.htm](classfeatures/sorcerer-01-o39zQMIdERWtmBSB.htm)|Bloodline: Diabolic|Lignage : Diabolique|changé|
|[sorcerer-01-PpzH9tJULk5ksX9w.htm](classfeatures/sorcerer-01-PpzH9tJULk5ksX9w.htm)|Bloodline: Psychopomp|Lignage : Psychopompe|libre|
|[sorcerer-01-RXRnJcG4XSabZ35a.htm](classfeatures/sorcerer-01-RXRnJcG4XSabZ35a.htm)|Bloodline: Elemental|Lignage : Élémentaire|changé|
|[sorcerer-01-tYOMBiH3HbViNWwn.htm](classfeatures/sorcerer-01-tYOMBiH3HbViNWwn.htm)|Bloodline: Genie|Lignage : Génie|libre|
|[sorcerer-01-uoQOm41BVdSo6pAS.htm](classfeatures/sorcerer-01-uoQOm41BVdSo6pAS.htm)|Bloodline: Shadow|Lignage : Ombre|libre|
|[sorcerer-01-vhW3glAaEfq2DKrw.htm](classfeatures/sorcerer-01-vhW3glAaEfq2DKrw.htm)|Bloodline: Angelic|Lignage : Angélique|changé|
|[sorcerer-01-w5koctOVrEcpxTIq.htm](classfeatures/sorcerer-01-w5koctOVrEcpxTIq.htm)|Bloodline: Demonic|Lignage : Démoniaque|changé|
|[sorcerer-01-ZEtJJ5UOlV5oTWWp.htm](classfeatures/sorcerer-01-ZEtJJ5UOlV5oTWWp.htm)|Bloodline: Imperial|Lignage : Impérial|changé|
|[sorcerer-01-ZHabYxSgYK0XbjhM.htm](classfeatures/sorcerer-01-ZHabYxSgYK0XbjhM.htm)|Bloodline: Draconic|Lignage : Draconique|changé|
|[sorcerer-03-y2vUZSYIYBp86UJc.htm](classfeatures/sorcerer-03-y2vUZSYIYBp86UJc.htm)|Signature Spells (Sorcerer)|Sorts emblématiques (Ensorceleur)|libre|
|[sorcerer-05-OSq5qxRiKvrYtAlC.htm](classfeatures/sorcerer-05-OSq5qxRiKvrYtAlC.htm)|Magical Fortitude (Sorcerer)|Vigueur magique (Ensorceleur)|officielle|
|[sorcerer-07-I2qauyUmyTH3coAU.htm](classfeatures/sorcerer-07-I2qauyUmyTH3coAU.htm)|Expert Spellcaster (Sorcerer)|Incantateur expert (Ensorceleur)|officielle|
|[sorcerer-09-QqcRWjr0Xc5xWVMR.htm](classfeatures/sorcerer-09-QqcRWjr0Xc5xWVMR.htm)|Lightning Reflexes (Sorcerer)|Réflexes éclair (Ensorceleur)|officielle|
|[sorcerer-11-4uvohEHSm17wY7gc.htm](classfeatures/sorcerer-11-4uvohEHSm17wY7gc.htm)|Simple Weapon Expertise|Expertise avec les armes simples|officielle|
|[sorcerer-11-Nr7U1P4IPu38rynd.htm](classfeatures/sorcerer-11-Nr7U1P4IPu38rynd.htm)|Alertness (Sorcerer)|Vigilance (Ensorceleur)|libre|
|[sorcerer-13-n6ItiwN9cGIK6n78.htm](classfeatures/sorcerer-13-n6ItiwN9cGIK6n78.htm)|Weapon Specialization (Sorcerer)|Spécialisation martiale (Ensorceleur)|officielle|
|[sorcerer-13-t8AfHFWqv5vCZ5uJ.htm](classfeatures/sorcerer-13-t8AfHFWqv5vCZ5uJ.htm)|Defensive Robes (Sorcerer)|Robes défensives (Ensorceleur)|officielle|
|[sorcerer-15-Rt3Jlxk2GgwkIGLm.htm](classfeatures/sorcerer-15-Rt3Jlxk2GgwkIGLm.htm)|Master Spellcaster (Sorcerer)|Maître incantateur (Ensorceleur)|officielle|
|[sorcerer-17-eC8PhLpgho84tGcD.htm](classfeatures/sorcerer-17-eC8PhLpgho84tGcD.htm)|Resolve (Sorcerer)|Résolution (Ensorceleur)|officielle|
|[sorcerer-19-EgJFG9DNs9jNuDL9.htm](classfeatures/sorcerer-19-EgJFG9DNs9jNuDL9.htm)|Legendary Spellcaster (Sorcerer)|Incantateur légendaire (Ensorceleur)|officielle|
|[sorcerer-19-feCnVrPPlKhl701x.htm](classfeatures/sorcerer-19-feCnVrPPlKhl701x.htm)|Bloodline Paragon|Parangon du lignage|officielle|
|[swashbuckler-01-4lGhbEjlEoGP4scl.htm](classfeatures/swashbuckler-01-4lGhbEjlEoGP4scl.htm)|Wit Style|Style : Esprit|libre|
|[swashbuckler-01-5HoEwzLDJGTCZtFa.htm](classfeatures/swashbuckler-01-5HoEwzLDJGTCZtFa.htm)|Battledancer Style|Style : Danseur de combat|libre|
|[swashbuckler-01-B7RMnrHwQHlezlJT.htm](classfeatures/swashbuckler-01-B7RMnrHwQHlezlJT.htm)|Gymnast Style|Style : Gymnaste|libre|
|[swashbuckler-01-beW1OqibVQ3fBvRw.htm](classfeatures/swashbuckler-01-beW1OqibVQ3fBvRw.htm)|Swashbuckler's Style|Style du bretteur|libre|
|[swashbuckler-01-Jgid6Ja6Y879COlN.htm](classfeatures/swashbuckler-01-Jgid6Ja6Y879COlN.htm)|Fencer Style|Style : escrimeur|libre|
|[swashbuckler-01-KBhwFjdptrKyN5EM.htm](classfeatures/swashbuckler-01-KBhwFjdptrKyN5EM.htm)|Braggart Style|Style : fanfaron|libre|
|[swashbuckler-01-LzYi0OuOoypNb6jd.htm](classfeatures/swashbuckler-01-LzYi0OuOoypNb6jd.htm)|Panache|Panache|libre|
|[swashbuckler-01-pyo0vmxUFIFX2GNl.htm](classfeatures/swashbuckler-01-pyo0vmxUFIFX2GNl.htm)|Confident Finisher|Coup final assuré|libre|
|[swashbuckler-01-RQH6vigvhmiYKKjg.htm](classfeatures/swashbuckler-01-RQH6vigvhmiYKKjg.htm)|Precise Strike|Frappe précise|libre|
|[swashbuckler-03-8BOFeRE7ZfJ02N0O.htm](classfeatures/swashbuckler-03-8BOFeRE7ZfJ02N0O.htm)|Vivacious Speed|Vitesse exubérante|libre|
|[swashbuckler-03-Jtn7IugykXDlIoZq.htm](classfeatures/swashbuckler-03-Jtn7IugykXDlIoZq.htm)|Opportune Riposte|riposte opportune|libre|
|[swashbuckler-03-pthjQIK9pDxnbER6.htm](classfeatures/swashbuckler-03-pthjQIK9pDxnbER6.htm)|Stylish Tricks|Astuces de style|libre|
|[swashbuckler-03-TySxvlssa9YjQhjJ.htm](classfeatures/swashbuckler-03-TySxvlssa9YjQhjJ.htm)|Great Fortitude (Swashbuckler)|Vigueur surhumaine (Bretteur)|libre|
|[swashbuckler-05-F5BHEav90oOJ2LwN.htm](classfeatures/swashbuckler-05-F5BHEav90oOJ2LwN.htm)|Weapon Expertise (Swashbuckler)|Expertise martiale (Bretteur)|libre|
|[swashbuckler-07-GdEYWajctNsihg1z.htm](classfeatures/swashbuckler-07-GdEYWajctNsihg1z.htm)|Weapon Specialization (Swashbuckler)|Spécialisation martiale (Bretteur)|libre|
|[swashbuckler-07-W4iCc24gNXPY67SS.htm](classfeatures/swashbuckler-07-W4iCc24gNXPY67SS.htm)|Evasion (Swashbuckler)|Évasion (Bretteur)|libre|
|[swashbuckler-09-KxpaxUSuBC7hr4F7.htm](classfeatures/swashbuckler-09-KxpaxUSuBC7hr4F7.htm)|Exemplary Finisher|Coup final exemplaire|libre|
|[swashbuckler-09-U74JoAcLHTOsZG6q.htm](classfeatures/swashbuckler-09-U74JoAcLHTOsZG6q.htm)|Swashbuckler Expertise|Expertise du bretteur|libre|
|[swashbuckler-11-13QpCrR8a8XULbJa.htm](classfeatures/swashbuckler-11-13QpCrR8a8XULbJa.htm)|Continuous Flair|Élégance continuelle|changé|
|[swashbuckler-11-6JYLXF2L3tq7aN5O.htm](classfeatures/swashbuckler-11-6JYLXF2L3tq7aN5O.htm)|Vigilant Senses (Swashbuckler)|Sens alertes (Bretteur)|libre|
|[swashbuckler-13-dfmcFY6SqPMkqbQp.htm](classfeatures/swashbuckler-13-dfmcFY6SqPMkqbQp.htm)|Weapon Mastery (Swashbuckler)|Maîtrise martiale (Bretteur)|libre|
|[swashbuckler-13-FCWt3xFQeINVY8JW.htm](classfeatures/swashbuckler-13-FCWt3xFQeINVY8JW.htm)|Light Armor Expertise (Swashbuckler)|Expertise avec les armures légères (Bretteur)|libre|
|[swashbuckler-13-oiA05VSn8Q9kiF8B.htm](classfeatures/swashbuckler-13-oiA05VSn8Q9kiF8B.htm)|Improved Evasion (Swashbuckler)|Évasion améliorée (Bretteur)|libre|
|[swashbuckler-15-nxV6vahBSyv7gNSc.htm](classfeatures/swashbuckler-15-nxV6vahBSyv7gNSc.htm)|Greater Weapon Specialization (Swashbuckler)|Spécialisation martiale supérieure (Bretteur)|libre|
|[swashbuckler-15-Pk3Ht0KZyFxSeL07.htm](classfeatures/swashbuckler-15-Pk3Ht0KZyFxSeL07.htm)|Keen Flair|Élégance aigüe|libre|
|[swashbuckler-17-7qJIXNnkFDOWVtNs.htm](classfeatures/swashbuckler-17-7qJIXNnkFDOWVtNs.htm)|Resolve (Swashbuckler)|Résolution (Bretteur)|libre|
|[swashbuckler-19-G0Sq4tt26TeneUs1.htm](classfeatures/swashbuckler-19-G0Sq4tt26TeneUs1.htm)|Light Armor Mastery (Swashbuckler)|Maîtrise des armures légères (Bretteur)|libre|
|[swashbuckler-19-ypfT3iybew6ZSIUl.htm](classfeatures/swashbuckler-19-ypfT3iybew6ZSIUl.htm)|Eternal Confidence|Confiance éternelle|libre|
|[witch-01-4IfYHrQMosJNM8hv.htm](classfeatures/witch-01-4IfYHrQMosJNM8hv.htm)|Fervor Patron|Patron : Ferveur|libre|
|[witch-01-9uLh5z2uPo6LDFRY.htm](classfeatures/witch-01-9uLh5z2uPo6LDFRY.htm)|Hexes|Maléfices|libre|
|[witch-01-ejmSQOJR5lJv1pzh.htm](classfeatures/witch-01-ejmSQOJR5lJv1pzh.htm)|Rune Patron|Patron : Rune|libre|
|[witch-01-KPtF29AaeX2sJW0K.htm](classfeatures/witch-01-KPtF29AaeX2sJW0K.htm)|Patron|Patron|libre|
|[witch-01-NAXRmMjj0gcyD7ie.htm](classfeatures/witch-01-NAXRmMjj0gcyD7ie.htm)|Curse Patron|Patron : Malédiction|libre|
|[witch-01-nocYmxbi4rqCC2qS.htm](classfeatures/witch-01-nocYmxbi4rqCC2qS.htm)|Patron Theme|Thême de patron|libre|
|[witch-01-qf12ubZ07Q0z0NcN.htm](classfeatures/witch-01-qf12ubZ07Q0z0NcN.htm)|Winter Patron|Patron : Hiver|libre|
|[witch-01-qMZiTugiLCEmkg8h.htm](classfeatures/witch-01-qMZiTugiLCEmkg8h.htm)|Fate Patron|Patron : Destin|libre|
|[witch-01-SOan0fqyFTrkqJLV.htm](classfeatures/witch-01-SOan0fqyFTrkqJLV.htm)|Witch Lessons|Leçons de sorcière|libre|
|[witch-01-VVMMJdIWL7fAsQf3.htm](classfeatures/witch-01-VVMMJdIWL7fAsQf3.htm)|Baba Yaga Patron|Patron : Baba Yaga|libre|
|[witch-01-x2gzQMPvLwHWDdAC.htm](classfeatures/witch-01-x2gzQMPvLwHWDdAC.htm)|Wild Patron|Patron : Sauvage|libre|
|[witch-01-XFTWJO6txmLNRLae.htm](classfeatures/witch-01-XFTWJO6txmLNRLae.htm)|Night Patron|Patron : Nuit|libre|
|[witch-01-yksPhweBZYVCsE1A.htm](classfeatures/witch-01-yksPhweBZYVCsE1A.htm)|Familiar (Witch)|Familier (Sorcière)|libre|
|[witch-01-zT6QiTMxxj8JYoN9.htm](classfeatures/witch-01-zT6QiTMxxj8JYoN9.htm)|Witch Spellcasting|Incantation de sorcière|libre|
|[witch-05-zXVHXoZKpTootce0.htm](classfeatures/witch-05-zXVHXoZKpTootce0.htm)|Magical Fortitude (Witch)|Vigueur magique (Sorcière)|libre|
|[witch-07-jSw13ajIzbUdiwwA.htm](classfeatures/witch-07-jSw13ajIzbUdiwwA.htm)|Expert Spellcaster (Witch)|Incantateur expert (Sorcière)|libre|
|[witch-09-uW7EyPyjgmDPL5Bg.htm](classfeatures/witch-09-uW7EyPyjgmDPL5Bg.htm)|Lightning Reflexes (Witch)|Réflexes éclair (Sorcière)|libre|
|[witch-11-E3dRpCVIs2daBJs1.htm](classfeatures/witch-11-E3dRpCVIs2daBJs1.htm)|Alertness (Witch)|Vigilance (Sorcière)|libre|
|[witch-11-Vnt4Yx2Eon9n8Rgw.htm](classfeatures/witch-11-Vnt4Yx2Eon9n8Rgw.htm)|Weapon Expertise (Witch)|Expertise martiale (Sorcière)|libre|
|[witch-13-8j0C9BkwO7wa125a.htm](classfeatures/witch-13-8j0C9BkwO7wa125a.htm)|Weapon Specialization (Witch)|Spécialisation martiale (Sorcière)|libre|
|[witch-13-D21HbZu4lssomcG6.htm](classfeatures/witch-13-D21HbZu4lssomcG6.htm)|Defensive Robes (Witch)|Robes défensives (Sorcière)|libre|
|[witch-15-IDtk2iV6haZWEhQj.htm](classfeatures/witch-15-IDtk2iV6haZWEhQj.htm)|Master Spellcaster (Witch)|Maître Incantateur (Sorcière)|libre|
|[witch-17-hNy2K2QmKF6sHA2A.htm](classfeatures/witch-17-hNy2K2QmKF6sHA2A.htm)|Resolve (Witch)|Résolution (Sorcière)|libre|
|[witch-19-3NrNkKwAwnHT0zJE.htm](classfeatures/witch-19-3NrNkKwAwnHT0zJE.htm)|Legendary Spellcaster (Witch)|Incantateur légendaire (Sorcière)|libre|
|[witch-19-cDnFXfl3i5Z2l7JP.htm](classfeatures/witch-19-cDnFXfl3i5Z2l7JP.htm)|Patron's Gift|Don du patron|libre|
|[wizard-00-S6WW4Yyg4XonXGHD.htm](classfeatures/wizard-00-S6WW4Yyg4XonXGHD.htm)|Arcane Spellcasting|Incantation arcanique|officielle|
|[wizard-01-89zWKD2CN7nRu2xp.htm](classfeatures/wizard-01-89zWKD2CN7nRu2xp.htm)|Arcane Thesis: Metamagical Experimentation|Thèse arcanique : Expérimentation métamagique|officielle|
|[wizard-01-au0lwQ1nAcNQwcGh.htm](classfeatures/wizard-01-au0lwQ1nAcNQwcGh.htm)|Arcane Bond|Lien arcanique|officielle|
|[wizard-01-gCwcys8CnS102tji.htm](classfeatures/wizard-01-gCwcys8CnS102tji.htm)|Arcane School: Abjuration|École arcanique : Abjuration|officielle|
|[wizard-01-ibhml5y20g5M3Vgd.htm](classfeatures/wizard-01-ibhml5y20g5M3Vgd.htm)|Arcane School: Evocation|École arcanique : Évocation|officielle|
|[wizard-01-K6hG7nH8yjmbA0Q9.htm](classfeatures/wizard-01-K6hG7nH8yjmbA0Q9.htm)|Arcane School: Illusion|École arcanique : Illusion|officielle|
|[wizard-01-Klb35AwlkNrq1gpB.htm](classfeatures/wizard-01-Klb35AwlkNrq1gpB.htm)|Arcane Thesis: Staff Nexus|Thèse arcanique : Bâton nexus|libre|
|[wizard-01-M89l9FOnjHe63wD7.htm](classfeatures/wizard-01-M89l9FOnjHe63wD7.htm)|Arcane Thesis|Thèse arcanique|libre|
|[wizard-01-OAcxS625AXSGrQIC.htm](classfeatures/wizard-01-OAcxS625AXSGrQIC.htm)|Arcane Thesis: Spell Blending|Thèse arcanique : Mélange de sorts|officielle|
|[wizard-01-qczCKdg47eAmCOUD.htm](classfeatures/wizard-01-qczCKdg47eAmCOUD.htm)|Arcane School: Universalist|École arcanique : universaliste|officielle|
|[wizard-01-QzWXMCSGNfvvpYgF.htm](classfeatures/wizard-01-QzWXMCSGNfvvpYgF.htm)|Arcane Thesis: Spell Substitution|Thèse arcanique : Substitution de sort|officielle|
|[wizard-01-rHxkPijLnQ9O9AGV.htm](classfeatures/wizard-01-rHxkPijLnQ9O9AGV.htm)|Arcane School: Transmutation|École arcanique : Transmutation|officielle|
|[wizard-01-SNeVaUBTHwvoO6kr.htm](classfeatures/wizard-01-SNeVaUBTHwvoO6kr.htm)|Arcane School: Conjuration|École arcanique : Invocation|officielle|
|[wizard-01-SNZ46g3u7U6x0XJj.htm](classfeatures/wizard-01-SNZ46g3u7U6x0XJj.htm)|Arcane Thesis: Improved Familiar Attunement|Thèse arcanique : Amélioration de l’harmonisation avec le familier|officielle|
|[wizard-01-uNM7qZQokRKAEd7k.htm](classfeatures/wizard-01-uNM7qZQokRKAEd7k.htm)|Arcane School: Necromancy|École arcanique : Nécromancie|officielle|
|[wizard-01-yobGgrHdgjs5QW5o.htm](classfeatures/wizard-01-yobGgrHdgjs5QW5o.htm)|Arcane School: Divination|École arcanique : Divination|officielle|
|[wizard-01-ZHwGACWQEy6kTzcP.htm](classfeatures/wizard-01-ZHwGACWQEy6kTzcP.htm)|Arcane School: Enchantment|École arcanique : Enchantement|officielle|
|[wizard-05-8QQxWTxbkae0fJCI.htm](classfeatures/wizard-05-8QQxWTxbkae0fJCI.htm)|Lightning Reflexes (Wizard)|Réflexes éclair (Magicien)|officielle|
|[wizard-07-hTAOBpNfnGwukfLG.htm](classfeatures/wizard-07-hTAOBpNfnGwukfLG.htm)|Expert Spellcaster (Wizard)|Incantateur expert (Magicien)|officielle|
|[wizard-09-QZCJBN6J4cAeOV8O.htm](classfeatures/wizard-09-QZCJBN6J4cAeOV8O.htm)|Magical Fortitude (Wizard)|Vigueur magique (Magicien)|officielle|
|[wizard-11-GBsC2cARoFiqMi9V.htm](classfeatures/wizard-11-GBsC2cARoFiqMi9V.htm)|Wizard Weapon Expertise|Expertise avec les armes du magicien|officielle|
|[wizard-11-wsVAwcs3j6k8I4tP.htm](classfeatures/wizard-11-wsVAwcs3j6k8I4tP.htm)|Alertness (Wizard)|Vigilance (Magicien)|officielle|
|[wizard-13-DbAj1y5vjxUgYLEJ.htm](classfeatures/wizard-13-DbAj1y5vjxUgYLEJ.htm)|Weapon Specialization (Wizard)|Spécialisation martiale (Magicien)|officielle|
|[wizard-13-R0nGdOlc4KCtdJJr.htm](classfeatures/wizard-13-R0nGdOlc4KCtdJJr.htm)|Defensive Robes (Wizard)|Robes défensives (Magicien)|officielle|
|[wizard-15-9chVicXrB0pvpUjH.htm](classfeatures/wizard-15-9chVicXrB0pvpUjH.htm)|Master Spellcaster (Wizard)|Maître incantateur (Magicien)|officielle|
|[wizard-17-4WUJl1RQMpKBFaz0.htm](classfeatures/wizard-17-4WUJl1RQMpKBFaz0.htm)|Resolve (Wizard)|Résolution (Magicien)|officielle|
|[wizard-19-ilIZDX2q7X87GraR.htm](classfeatures/wizard-19-ilIZDX2q7X87GraR.htm)|Legendary Spellcaster (Wizard)|Incantateur légendaire (Magicien)|officielle|
|[wizard-19-ZjwJHmjPrSs6VDez.htm](classfeatures/wizard-19-ZjwJHmjPrSs6VDez.htm)|Archwizard's Spellcraft|Art magique de l'archimage|officielle|
