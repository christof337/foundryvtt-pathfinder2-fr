# État de la traduction (archetypes)

 * **aucune**: 74
 * **libre**: 1


Dernière mise à jour: 2020-11-16 19:02 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[25NJcH5SkWh1Tol7.htm](archetypes/25NJcH5SkWh1Tol7.htm)|Spellmaster|
|[2eAiIynDWTMKTGDU.htm](archetypes/2eAiIynDWTMKTGDU.htm)|Scrollmaster|
|[2Wol7CpOBJjVT3aP.htm](archetypes/2Wol7CpOBJjVT3aP.htm)|Hellknight|
|[3jceS1GfWcSQ3tMY.htm](archetypes/3jceS1GfWcSQ3tMY.htm)|Talisman Dabbler|
|[4xzvjFVytFA0LIYj.htm](archetypes/4xzvjFVytFA0LIYj.htm)|Living Monolith|
|[5cbEwgPLlMWGGVxN.htm](archetypes/5cbEwgPLlMWGGVxN.htm)|Dandy|
|[5zILvODoiQC34tWU.htm](archetypes/5zILvODoiQC34tWU.htm)|Blessed One|
|[678FjGfCmxuNbNhe.htm](archetypes/678FjGfCmxuNbNhe.htm)|Lion Blade|
|[70DYBk9gZCH9uSUs.htm](archetypes/70DYBk9gZCH9uSUs.htm)|Shadowdancer|
|[7BNWl18efHJ93hke.htm](archetypes/7BNWl18efHJ93hke.htm)|Assassin|
|[8vuL2hCPrcG2duLt.htm](archetypes/8vuL2hCPrcG2duLt.htm)|Juggler|
|[akmaoP0StWvIv3jb.htm](archetypes/akmaoP0StWvIv3jb.htm)|Oracle|
|[bDP7kO6bnQh6no0C.htm](archetypes/bDP7kO6bnQh6no0C.htm)|Bright Lion|
|[bi1SeBS7Af3ZisIA.htm](archetypes/bi1SeBS7Af3ZisIA.htm)|Swordmaster|
|[Bl0KM29OOlwBegCi.htm](archetypes/Bl0KM29OOlwBegCi.htm)|Familiar Master|
|[Buptz08MArCtY41w.htm](archetypes/Buptz08MArCtY41w.htm)|Investigator|
|[CA22ZhzFPjahrO4W.htm](archetypes/CA22ZhzFPjahrO4W.htm)|Medic|
|[cEiMI4QGqjv69pJ6.htm](archetypes/cEiMI4QGqjv69pJ6.htm)|Herbalist|
|[cnsq5cXgQu4FXnoZ.htm](archetypes/cnsq5cXgQu4FXnoZ.htm)|Aldori Duelist|
|[DeixUOfwL7Bruad5.htm](archetypes/DeixUOfwL7Bruad5.htm)|Red Mantis Assassin|
|[DMsE43xgf1gHPl8o.htm](archetypes/DMsE43xgf1gHPl8o.htm)|Loremaster|
|[DYJfhMcLlP05oMDD.htm](archetypes/DYJfhMcLlP05oMDD.htm)|Duelist|
|[e2KCqgD3zt8yvxGg.htm](archetypes/e2KCqgD3zt8yvxGg.htm)|Sentinel|
|[eEyxTQ3oHfthdg0e.htm](archetypes/eEyxTQ3oHfthdg0e.htm)|Scrounger|
|[fjyB6HdS95xHEQn0.htm](archetypes/fjyB6HdS95xHEQn0.htm)|Poisoner|
|[GADKhvTRWCqlI9vy.htm](archetypes/GADKhvTRWCqlI9vy.htm)|Oozemorph|
|[GCoO342NLbrmEer1.htm](archetypes/GCoO342NLbrmEer1.htm)|Bounty Hunter|
|[GQn5HfP1Jg5qO0Dz.htm](archetypes/GQn5HfP1Jg5qO0Dz.htm)|Swashbuckler|
|[gw4J1pXYED71TPWx.htm](archetypes/gw4J1pXYED71TPWx.htm)|Bellflower Tiller|
|[GwItivMACzBmW82g.htm](archetypes/GwItivMACzBmW82g.htm)|Hellknight Signifer|
|[hLFBAs2NpyGjQiDA.htm](archetypes/hLFBAs2NpyGjQiDA.htm)|Cavalier|
|[IoDeWTPMoa7LNLWD.htm](archetypes/IoDeWTPMoa7LNLWD.htm)|Ritualist|
|[IU0WKnP9qciXztOC.htm](archetypes/IU0WKnP9qciXztOC.htm)|Gladiator|
|[JiRqSMv1STETLwSP.htm](archetypes/JiRqSMv1STETLwSP.htm)|Student of Perfection|
|[K1mmFJ3QSjNkHm2L.htm](archetypes/K1mmFJ3QSjNkHm2L.htm)|Dual-Weapon Warrior|
|[kisnuKPMGheORROU.htm](archetypes/kisnuKPMGheORROU.htm)|Animal Trainer|
|[l4sKdl0ub0UGBwsb.htm](archetypes/l4sKdl0ub0UGBwsb.htm)|Knight Vigilant|
|[Ld3wizivrctmoHhy.htm](archetypes/Ld3wizivrctmoHhy.htm)|Martial Artist|
|[Lr108TWazbuxxUzP.htm](archetypes/Lr108TWazbuxxUzP.htm)|Zephyr Guard|
|[lsU4NQEei111OetM.htm](archetypes/lsU4NQEei111OetM.htm)|Dragon Disciple|
|[LYYDozZfXok19VYW.htm](archetypes/LYYDozZfXok19VYW.htm)|Hellknight Armiger|
|[MJ2EBeAVkc61mznR.htm](archetypes/MJ2EBeAVkc61mznR.htm)|Archer|
|[myvFp45VnA2FPOvG.htm](archetypes/myvFp45VnA2FPOvG.htm)|Archaeologist|
|[NGrKVIaZJx3r3In4.htm](archetypes/NGrKVIaZJx3r3In4.htm)|Weapon Improviser|
|[o3C85NHW08Y2inAN.htm](archetypes/o3C85NHW08Y2inAN.htm)|Pathfinder Agent|
|[OmEG0nhQgeKnAUsn.htm](archetypes/OmEG0nhQgeKnAUsn.htm)|Magic Warrior|
|[pHTnfVHOzsgfZ9E2.htm](archetypes/pHTnfVHOzsgfZ9E2.htm)|Scout|
|[PlPc4w1XG9VOpDgn.htm](archetypes/PlPc4w1XG9VOpDgn.htm)|Pirate|
|[Q3VydQ6b5Mxm1c41.htm](archetypes/Q3VydQ6b5Mxm1c41.htm)|Edgewatch Detective|
|[Qsmzm6w1ZNpHX7GD.htm](archetypes/Qsmzm6w1ZNpHX7GD.htm)|Halcyon Speaker|
|[R0ORjHAeQ0auxl2j.htm](archetypes/R0ORjHAeQ0auxl2j.htm)|Vigilante|
|[RvjlaAPa2Wkdhrms.htm](archetypes/RvjlaAPa2Wkdhrms.htm)|Horizon Walker|
|[RyjrUkqSIg52Mjl8.htm](archetypes/RyjrUkqSIg52Mjl8.htm)|Runescarred|
|[s7kWIaeCdj3IFte9.htm](archetypes/s7kWIaeCdj3IFte9.htm)|Jalmeri Heavenseeker|
|[SAyPnVRucJa8k4MN.htm](archetypes/SAyPnVRucJa8k4MN.htm)|Witch|
|[SbvhfCo5gFVt1DgD.htm](archetypes/SbvhfCo5gFVt1DgD.htm)|Acrobat|
|[SJi2cicLd7R2FInl.htm](archetypes/SJi2cicLd7R2FInl.htm)|Viking|
|[TdoPhja8JA9C2Itm.htm](archetypes/TdoPhja8JA9C2Itm.htm)|Linguist|
|[ThxMkphglPJuweXB.htm](archetypes/ThxMkphglPJuweXB.htm)|Celebrity|
|[uKoay1d62vRae3z1.htm](archetypes/uKoay1d62vRae3z1.htm)|Snarecrafter|
|[UOhVrWvuaAVtQHEo.htm](archetypes/UOhVrWvuaAVtQHEo.htm)|Mauler|
|[UsT8XqN5LwfbBMzp.htm](archetypes/UsT8XqN5LwfbBMzp.htm)|Bastion|
|[Utxq3fo8AoPyfh4K.htm](archetypes/Utxq3fo8AoPyfh4K.htm)|Lastwall Sentry|
|[VRUYc3QyRjjOjppc.htm](archetypes/VRUYc3QyRjjOjppc.htm)|Turpin Rowe Lumberjack|
|[WrQ2Qpv958C6cfD0.htm](archetypes/WrQ2Qpv958C6cfD0.htm)|Knight Reclaimant|
|[XATPzPTZ9Je89erT.htm](archetypes/XATPzPTZ9Je89erT.htm)|Golem Grafter|
|[xFjXKdIkA574QRkv.htm](archetypes/xFjXKdIkA574QRkv.htm)|Scroll Trickster|
|[xNzNpPjeNSrXE7WG.htm](archetypes/xNzNpPjeNSrXE7WG.htm)|Crystal Keeper|
|[XZwGLLgC1sIKlR1c.htm](archetypes/XZwGLLgC1sIKlR1c.htm)|Staff Acrobat|
|[YBvXmJ5e9MThdws0.htm](archetypes/YBvXmJ5e9MThdws0.htm)|Firebrand Braggart|
|[yvb2pY3Qb7Jl35hk.htm](archetypes/yvb2pY3Qb7Jl35hk.htm)|Magaambyan Attendant|
|[z5yvyY2HfIRgwGnO.htm](archetypes/z5yvyY2HfIRgwGnO.htm)|Eldrich Archer|
|[zv31F34JgUesREuz.htm](archetypes/zv31F34JgUesREuz.htm)|Marshal|
|[ZvIreFl9TXgrj52Q.htm](archetypes/ZvIreFl9TXgrj52Q.htm)|Beastmaster|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[YJQJxskQf8VqDt8G.htm](archetypes/YJQJxskQf8VqDt8G.htm)|Provocator|Provocator|libre|
