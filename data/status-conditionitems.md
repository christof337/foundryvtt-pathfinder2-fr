# État de la traduction (conditionitems)

 * **changé**: 39
 * **officielle**: 3


Dernière mise à jour: 2020-11-16 19:02 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1wQY3JYyhMYeeV2G.htm](conditionitems/1wQY3JYyhMYeeV2G.htm)|Observed|Observé|changé|
|[3uh1r86TzbQvosxv.htm](conditionitems/3uh1r86TzbQvosxv.htm)|Doomed|Condamné|changé|
|[4D2KBtexWXa6oUMR.htm](conditionitems/4D2KBtexWXa6oUMR.htm)|Drained|Drainé|changé|
|[6dNUvdb1dhToNDj3.htm](conditionitems/6dNUvdb1dhToNDj3.htm)|Broken|Brisé|changé|
|[6uEgoh53GbXuHpTF.htm](conditionitems/6uEgoh53GbXuHpTF.htm)|Paralyzed|Paralysé|changé|
|[9evPzg9E6muFcoSk.htm](conditionitems/9evPzg9E6muFcoSk.htm)|Unnoticed|Inaperçu|changé|
|[9PR9y0bi4JPKnHPR.htm](conditionitems/9PR9y0bi4JPKnHPR.htm)|Deafened|Sourd|officielle|
|[9qGBRpbX9NEwtAAr.htm](conditionitems/9qGBRpbX9NEwtAAr.htm)|Controlled|Contrôlé|changé|
|[AdPVz7rbaVSRxHFg.htm](conditionitems/AdPVz7rbaVSRxHFg.htm)|Fascinated|Fasciné|changé|
|[AJh5ex99aV6VTggg.htm](conditionitems/AJh5ex99aV6VTggg.htm)|Flat-Footed|Pris au dépourvu|changé|
|[D5mg6Tc7Jzrj6ro7.htm](conditionitems/D5mg6Tc7Jzrj6ro7.htm)|Encumbered|Surchargé|changé|
|[dfCMdR4wnpbYNTix.htm](conditionitems/dfCMdR4wnpbYNTix.htm)|Stunned|Étourdi|changé|
|[DmAIPqOBomZ7H95W.htm](conditionitems/DmAIPqOBomZ7H95W.htm)|Concealed|Masqué|changé|
|[dTwPJuKgBQCMxixg.htm](conditionitems/dTwPJuKgBQCMxixg.htm)|Petrified|Pétrifié|changé|
|[e1XGnhKNSQIm5IXg.htm](conditionitems/e1XGnhKNSQIm5IXg.htm)|Stupefied|Stupéfié|changé|
|[eIcWbB5o3pP6OIMe.htm](conditionitems/eIcWbB5o3pP6OIMe.htm)|Immobilized|Immobilisé|changé|
|[fBnFDH2MTzgFijKf.htm](conditionitems/fBnFDH2MTzgFijKf.htm)|Unconscious|Inconscient|changé|
|[fesd1n5eVhpCSS18.htm](conditionitems/fesd1n5eVhpCSS18.htm)|Sickened|Malade|changé|
|[fuG8dgthlDWfWjIA.htm](conditionitems/fuG8dgthlDWfWjIA.htm)|Indifferent|Indifférent|changé|
|[HL2l2VRSaQHu9lUw.htm](conditionitems/HL2l2VRSaQHu9lUw.htm)|Fatigued|Fatigué|changé|
|[I1ffBVISxLr2gC4u.htm](conditionitems/I1ffBVISxLr2gC4u.htm)|Unfriendly|Inamical|changé|
|[i3OJZU2nk64Df3xm.htm](conditionitems/i3OJZU2nk64Df3xm.htm)|Clumsy|Maladroit|officielle|
|[iU0fEDdBp3rXpTMC.htm](conditionitems/iU0fEDdBp3rXpTMC.htm)|Hidden|Caché|changé|
|[j91X7x0XSomq8d60.htm](conditionitems/j91X7x0XSomq8d60.htm)|Prone|À terre|changé|
|[kWc1fhmv9LBiTuei.htm](conditionitems/kWc1fhmv9LBiTuei.htm)|Grabbed|Agrippé/empoigné|changé|
|[lDVqvLKA6eF3Df60.htm](conditionitems/lDVqvLKA6eF3Df60.htm)|Persistent Damage|Dégâts persistants|changé|
|[MIRkyAjyBeXivMa7.htm](conditionitems/MIRkyAjyBeXivMa7.htm)|Enfeebled|Affaibli|changé|
|[nlCjDvLMf2EkV2dl.htm](conditionitems/nlCjDvLMf2EkV2dl.htm)|Quickened|Accéléré|officielle|
|[sDPxOjQ9kx2RZE8D.htm](conditionitems/sDPxOjQ9kx2RZE8D.htm)|Fleeing|En fuite|changé|
|[TBSHQspnbcqxsmjL.htm](conditionitems/TBSHQspnbcqxsmjL.htm)|Frightened|Effrayé|changé|
|[TkIyaNPgTZFBCCuh.htm](conditionitems/TkIyaNPgTZFBCCuh.htm)|Dazzled|Ébloui|changé|
|[ud7gTLwPeklzYSXG.htm](conditionitems/ud7gTLwPeklzYSXG.htm)|Hostile|Hostile|changé|
|[v44P3WUcU1j0115l.htm](conditionitems/v44P3WUcU1j0115l.htm)|Helpful|Serviable|changé|
|[v66R7FdOf11l94im.htm](conditionitems/v66R7FdOf11l94im.htm)|Friendly|Amical|changé|
|[VcDeM8A5oI6VqhbM.htm](conditionitems/VcDeM8A5oI6VqhbM.htm)|Restrained|Entravé|changé|
|[VRSef5y1LmL2Hkjf.htm](conditionitems/VRSef5y1LmL2Hkjf.htm)|Undetected|Non détecté|changé|
|[XgEqL1kFApUbl5Z2.htm](conditionitems/XgEqL1kFApUbl5Z2.htm)|Blinded|Aveuglé|changé|
|[xYTAsEpcJE1Ccni3.htm](conditionitems/xYTAsEpcJE1Ccni3.htm)|Slowed|Ralenti|changé|
|[yblD8fOR1J8rDwEQ.htm](conditionitems/yblD8fOR1J8rDwEQ.htm)|Confused|Confus|changé|
|[Yl48xTdMh3aeQYL2.htm](conditionitems/Yl48xTdMh3aeQYL2.htm)|Wounded|Blessé|changé|
|[yZRUzMqrMmfLu0V1.htm](conditionitems/yZRUzMqrMmfLu0V1.htm)|Dying|Mourant|changé|
|[zJxUflt9np0q4yML.htm](conditionitems/zJxUflt9np0q4yML.htm)|Invisible|Invisible|changé|
