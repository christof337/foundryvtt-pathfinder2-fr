# État de la traduction (backgrounds)

 * **libre**: 143
 * **officielle**: 39
 * **aucune**: 6
 * **changé**: 5


Dernière mise à jour: 2020-11-16 19:02 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[45DuRopZt9dTJo3a.htm](backgrounds/45DuRopZt9dTJo3a.htm)|Translator|
|[CFdKZFcCcBEPjG5v.htm](backgrounds/CFdKZFcCcBEPjG5v.htm)|Harrow-Led|
|[lGXKBZYzT2IM69Yj.htm](backgrounds/lGXKBZYzT2IM69Yj.htm)|Pathfinder Recruiter|
|[RuQNK9zaRqpe45H9.htm](backgrounds/RuQNK9zaRqpe45H9.htm)|Trailblazer|
|[sPar7gD7KpbKcZLJ.htm](backgrounds/sPar7gD7KpbKcZLJ.htm)|Spell Seeker|
|[tkA82BeDl8NJO8nF.htm](backgrounds/tkA82BeDl8NJO8nF.htm)|Ex-Con Token Guard|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0EIhRniun8jfdPeN.htm](backgrounds/0EIhRniun8jfdPeN.htm)|Rostland Partisan|Partisan du Rost|libre|
|[0qrZaukJ0aIoPx3P.htm](backgrounds/0qrZaukJ0aIoPx3P.htm)|Iolite Trainee Hobgoblin|Recrue iolite hobgobeline|libre|
|[1bd25sY8k1moRcca.htm](backgrounds/1bd25sY8k1moRcca.htm)|Root Worker|Chasseur de racines|libre|
|[1zyrJ5dQPKedoSfP.htm](backgrounds/1zyrJ5dQPKedoSfP.htm)|Razmiran Faithful|Croyant en Razmir|libre|
|[2PfqXmTTdwWAJNoc.htm](backgrounds/2PfqXmTTdwWAJNoc.htm)|Diobel Pearl Diver|Pêcheur de perles de Diobel|libre|
|[3ljseFp6UbDGmCNn.htm](backgrounds/3ljseFp6UbDGmCNn.htm)|Emancipated|Émancipé|officielle|
|[3w6cSTa3Zc4z2ygu.htm](backgrounds/3w6cSTa3Zc4z2ygu.htm)|Aspiring River Monarch|Aspirant Monarque du Fleuve|libre|
|[4cYU0lSu5lDM9Qfc.htm](backgrounds/4cYU0lSu5lDM9Qfc.htm)|Harbor Guard Moonlighter|Noctambule de la Garde du port|libre|
|[5DnDnoSyyd1UPyRV.htm](backgrounds/5DnDnoSyyd1UPyRV.htm)|Touched by Dahak|Touché par Dahak|libre|
|[5eB5SefI6OlARL7q.htm](backgrounds/5eB5SefI6OlARL7q.htm)|Barrister|Avocat|officielle|
|[5KrCZHHMZmaM3FUl.htm](backgrounds/5KrCZHHMZmaM3FUl.htm)|Child of the Puddles|Marmot des flaques|libre|
|[5PsnYGatgMEYy9xH.htm](backgrounds/5PsnYGatgMEYy9xH.htm)|Cursed Family|Famille maudite|libre|
|[5u7FKCHuWmscNe5i.htm](backgrounds/5u7FKCHuWmscNe5i.htm)|Secular Medic|Médecin séculier|libre|
|[5vym5sPIxlDEEjGc.htm](backgrounds/5vym5sPIxlDEEjGc.htm)|Guard|Garde|officielle|
|[64WHEiUYl5Bp9Rmo.htm](backgrounds/64WHEiUYl5Bp9Rmo.htm)|Artist|Artiste|officielle|
|[6gFzKv1eBQJ1xaH9.htm](backgrounds/6gFzKv1eBQJ1xaH9.htm)|Winter's Child|Enfant de l'Hiver|libre|
|[76WshSVCoyTq3SRF.htm](backgrounds/76WshSVCoyTq3SRF.htm)|Cultist|Cultiste|libre|
|[7MKHFbZitRzTwiP0.htm](backgrounds/7MKHFbZitRzTwiP0.htm)|Dreamer of the Verdant Moon|Rêveur de la lune verdoyante|libre|
|[7woKMTCFU4NC06l5.htm](backgrounds/7woKMTCFU4NC06l5.htm)|Aiudara Seeker|Chercheur d'aiudara|libre|
|[8MoCaPm2wHF3r1yt.htm](backgrounds/8MoCaPm2wHF3r1yt.htm)|Trade Consortium Underling|Subalterne de consortium marchand|libre|
|[8p0gfy5xzpFZRyaG.htm](backgrounds/8p0gfy5xzpFZRyaG.htm)|Freed Slave of Absalom|Esclave libéré d'Absalom|libre|
|[8uc1ABJWOJ3gVCFD.htm](backgrounds/8uc1ABJWOJ3gVCFD.htm)|Mammoth Speaker|Porte parole mammouth|libre|
|[8YFofTyVgpmNQpbz.htm](backgrounds/8YFofTyVgpmNQpbz.htm)|Ward|Pupille|libre|
|[98YK8hpUVuDZCoAH.htm](backgrounds/98YK8hpUVuDZCoAH.htm)|Thassilonian Delver|Fouilleur du thassilon|libre|
|[9d5Mo7qNSpDyKWzY.htm](backgrounds/9d5Mo7qNSpDyKWzY.htm)|Finadar Leshy|Léchi de Finadar|libre|
|[9wfKz4tokaTWmHJg.htm](backgrounds/9wfKz4tokaTWmHJg.htm)|Merchant|Marchand|officielle|
|[AbPoIrl7CUSenbL8.htm](backgrounds/AbPoIrl7CUSenbL8.htm)|Herbalist|Herboriste|officielle|
|[aiiZdNhGey0Ov8Lb.htm](backgrounds/aiiZdNhGey0Ov8Lb.htm)|Tapestry Refugee|Réfugié de la Tapisserie|libre|
|[At7irukB3d0jzzad.htm](backgrounds/At7irukB3d0jzzad.htm)|Miner|Mineur|officielle|
|[At7nhCMEu2i2bKj9.htm](backgrounds/At7nhCMEu2i2bKj9.htm)|Shoanti Name Bearer|Porteur de nom Shoanti|libre|
|[Axidv6oBgolUazAt.htm](backgrounds/Axidv6oBgolUazAt.htm)|Pilgrim|Pélerin|libre|
|[BtTJJZ84ffpYHebV.htm](backgrounds/BtTJJZ84ffpYHebV.htm)|Post Guard of All Trades|Garde du poste touche-à-tout|libre|
|[BvtKgDDIKsue17uU.htm](backgrounds/BvtKgDDIKsue17uU.htm)|Haunted|Hanté|libre|
|[byu7FHvqmhAwYZFf.htm](backgrounds/byu7FHvqmhAwYZFf.htm)|Laborer|Manoeuvre|officielle|
|[BZ0Myq87KqoyTOEP.htm](backgrounds/BZ0Myq87KqoyTOEP.htm)|Nomad|Nomade|officielle|
|[c4Gcdcz5vDOc5NCk.htm](backgrounds/c4Gcdcz5vDOc5NCk.htm)|Quick|Rapide|libre|
|[ckPq968bni6JxpA6.htm](backgrounds/ckPq968bni6JxpA6.htm)|Bounty Hunter|Chasseur de primes|officielle|
|[CMswLSRLQ72tbu0u.htm](backgrounds/CMswLSRLQ72tbu0u.htm)|Geb Crusader|Croisé du Geb|libre|
|[cxgUge0LLCMoI70S.htm](backgrounds/cxgUge0LLCMoI70S.htm)|Returning Descendant|Descendant de retour|officielle|
|[d3Wsm7KdS5NBsMwf.htm](backgrounds/d3Wsm7KdS5NBsMwf.htm)|Barber|Barbier|libre|
|[DboMU7sSqrkSsZgI.htm](backgrounds/DboMU7sSqrkSsZgI.htm)|Scavenger|Récupérateur|libre|
|[dcArzJ4ylKFshbbp.htm](backgrounds/dcArzJ4ylKFshbbp.htm)|Animal Whisperer|Dresseur|officielle|
|[dKML9xcgWbrkguGd.htm](backgrounds/dKML9xcgWbrkguGd.htm)|Entertainer|Bateleur|officielle|
|[dPjFZlQG4dg13irs.htm](backgrounds/dPjFZlQG4dg13irs.htm)|Savior of Air|Sauveur de l'air|libre|
|[DW3mWZ4wepwNVfAN.htm](backgrounds/DW3mWZ4wepwNVfAN.htm)|Rigger|Gréeur|libre|
|[eBg1vNMc8uITswo0.htm](backgrounds/eBg1vNMc8uITswo0.htm)|Cursed|Maudit|libre|
|[EcjUeihqNUB1KGhk.htm](backgrounds/EcjUeihqNUB1KGhk.htm)|Nirmathi Guerrilla|Escarmoucheur Nirmathi|libre|
|[ecmPA86bz6221BWZ.htm](backgrounds/ecmPA86bz6221BWZ.htm)|Menagerie Dung Sweeper|Balayeur de fumier de la Ménagerie|libre|
|[ekEBKpFNZ0QVwywW.htm](backgrounds/ekEBKpFNZ0QVwywW.htm)|Press-Ganged|Forçat|libre|
|[ekypFYXEQvaw8dpG.htm](backgrounds/ekypFYXEQvaw8dpG.htm)|Bookkeeper|Comptable|libre|
|[elfIp8j9Z2oLqAL9.htm](backgrounds/elfIp8j9Z2oLqAL9.htm)|Teacher|Enseignant|libre|
|[ERpM6zFyJOFB31is.htm](backgrounds/ERpM6zFyJOFB31is.htm)|Outrider|Éclaireur|libre|
|[EvMNfrutZkTjUHVW.htm](backgrounds/EvMNfrutZkTjUHVW.htm)|Hermean Expatriate|Expatrié d'Herméa|libre|
|[EZ8J6yKeCICf6Oi7.htm](backgrounds/EZ8J6yKeCICf6Oi7.htm)|Lumber Consortium Laborer|Ouvrier du Consortium du Bois|libre|
|[f3S14uBriv8enmMQ.htm](backgrounds/f3S14uBriv8enmMQ.htm)|Artisan|Artisan|officielle|
|[FA6ADIwZ0IV1LZv1.htm](backgrounds/FA6ADIwZ0IV1LZv1.htm)|Circus Born|Circadien|libre|
|[fAibW4s5hrHEaLrT.htm](backgrounds/fAibW4s5hrHEaLrT.htm)|Child of Westcrown|Enfant de Couronne d'Ouest|libre|
|[FeYe3gQtkUsxpW1I.htm](backgrounds/FeYe3gQtkUsxpW1I.htm)|Warrior|Homme d'armes|officielle|
|[FjIDq4tzApEUhAeQ.htm](backgrounds/FjIDq4tzApEUhAeQ.htm)|Learned Guard Prodigy|Prodige de la Garde Instruite|libre|
|[G2o8mCXdYKl1tbXP.htm](backgrounds/G2o8mCXdYKl1tbXP.htm)|Desert Tracker|Pisteur du désert|libre|
|[G4akDE6k0GpQ3dpD.htm](backgrounds/G4akDE6k0GpQ3dpD.htm)|Hermean Heritor|Héritier d'Herméa|libre|
|[g5pva9jnXA5ZCRjC.htm](backgrounds/g5pva9jnXA5ZCRjC.htm)|Servant|Serviteur|libre|
|[gad0K8Kvxk4auPwM.htm](backgrounds/gad0K8Kvxk4auPwM.htm)|Faction Opportunist|Opportuniste des Factions|libre|
|[GAWMi5F6MXXWqOpR.htm](backgrounds/GAWMi5F6MXXWqOpR.htm)|Legendary Parents|Parents légendaires|libre|
|[gBPUW9JAARw3lsdr.htm](backgrounds/gBPUW9JAARw3lsdr.htm)|Belkzen Slayer|Tueur du Belkzen|libre|
|[GBvUIdUFCkBpZdp0.htm](backgrounds/GBvUIdUFCkBpZdp0.htm)|Missionary|Missionnaire|libre|
|[Gcn19QCeblDvsrdv.htm](backgrounds/Gcn19QCeblDvsrdv.htm)|Tinker|Bricoleur|officielle|
|[gGHi0N5bZNbGbxB4.htm](backgrounds/gGHi0N5bZNbGbxB4.htm)|Teamster|Conducteur d'attelages|libre|
|[Gj9PhLYrZCQxvuwh.htm](backgrounds/Gj9PhLYrZCQxvuwh.htm)|Perfection Seeker|À la recherche de la Perfection|libre|
|[gpB6yH1lvLiU03eJ.htm](backgrounds/gpB6yH1lvLiU03eJ.htm)|Early Explorer|Explorateur précoce|libre|
|[gRh3Scs2RvtJM8wq.htm](backgrounds/gRh3Scs2RvtJM8wq.htm)|Undercover Lotus Guard|Garde Lotus infiltré|libre|
|[gxl5BXQ6LPfMnC6o.htm](backgrounds/gxl5BXQ6LPfMnC6o.htm)|Truth Seeker|En quête de vérité|officielle|
|[H1tJhj6ZbkLxmK8a.htm](backgrounds/H1tJhj6ZbkLxmK8a.htm)|Rivethun Adherent|Adhérent du Rivethun|libre|
|[H1yku00yjYxnNU3p.htm](backgrounds/H1yku00yjYxnNU3p.htm)|Aspiring Free Captain|Aspirant capitaine libre|libre|
|[H859zofQzmGngQ2d.htm](backgrounds/H859zofQzmGngQ2d.htm)|Pathfinder Hopeful|Aspirant Éclaireur|libre|
|[hcC58ohDMUOgmuZp.htm](backgrounds/hcC58ohDMUOgmuZp.htm)|Grand Council Bureaucrat|Bureaucrate du Grand Conseil|libre|
|[hDUQWHMVi4pRh2D7.htm](backgrounds/hDUQWHMVi4pRh2D7.htm)|Lastwall Survivor|Survivant de Dernier-Rempart|libre|
|[hgHxXEuwmNSrXD9J.htm](backgrounds/hgHxXEuwmNSrXD9J.htm)|Onyx Trader|Marchand d'Onyx|libre|
|[HmC4y8ENahNKp5Tl.htm](backgrounds/HmC4y8ENahNKp5Tl.htm)|Inlander|Habitant de l'intérieur des terres|libre|
|[HOlyT1Y0fXS85u0j.htm](backgrounds/HOlyT1Y0fXS85u0j.htm)|Scout|Éclaireur|officielle|
|[hQiyj0YTMNqMNf1x.htm](backgrounds/hQiyj0YTMNqMNf1x.htm)|Aerialist|Trapéziste|libre|
|[HqrzcYujcS4UeJxJ.htm](backgrounds/HqrzcYujcS4UeJxJ.htm)|Blow-In|Nouveau-venu|libre|
|[hTTc1cWinSbtAdh1.htm](backgrounds/hTTc1cWinSbtAdh1.htm)|Refugee (APG)|Réfugié (MJA)|libre|
|[huFMFGKbBoPB4bRc.htm](backgrounds/huFMFGKbBoPB4bRc.htm)|Gladiator|Gladiateur|officielle|
|[Ih6KKS2DEzXxiJpX.htm](backgrounds/Ih6KKS2DEzXxiJpX.htm)|Gambler|Parieur|officielle|
|[ijldm5oRi8hNyoyP.htm](backgrounds/ijldm5oRi8hNyoyP.htm)|Senghor Sailor|Marin de Senghor|libre|
|[iPjaDshFtGP3uMO8.htm](backgrounds/iPjaDshFtGP3uMO8.htm)|Scholar of the Sky Key|Étudiant de la Clé du Ciel|libre|
|[IRh6qlZtN7OBKrek.htm](backgrounds/IRh6qlZtN7OBKrek.htm)|Ustalavic Academic|Étudiant ustalavien|libre|
|[j9k6UFTgWs0c97R0.htm](backgrounds/j9k6UFTgWs0c97R0.htm)|Feybound|Engagé avec les fées|libre|
|[JfOPdd8V2CuN9I9a.htm](backgrounds/JfOPdd8V2CuN9I9a.htm)|Bellflower Agent|Agent de la Campanule|libre|
|[JH77p9uU187caJxX.htm](backgrounds/JH77p9uU187caJxX.htm)|Out-of-Towner|De passage|officielle|
|[JNCNUsussLP4NZfC.htm](backgrounds/JNCNUsussLP4NZfC.htm)|Returned|Ressuscité|libre|
|[JNSg5UvBKdIgLNJ6.htm](backgrounds/JNSg5UvBKdIgLNJ6.htm)|Issian Partisan|Partisan Issien|libre|
|[JV5GjNjPuFWyTbw0.htm](backgrounds/JV5GjNjPuFWyTbw0.htm)|Wonder Taster|Goûteur de merveilles|libre|
|[K12klGJogLLgGLIw.htm](backgrounds/K12klGJogLLgGLIw.htm)|Whispering Way Scion|Enfant de la Voie du Murmure|libre|
|[KbPsIOy2v6c7H87i.htm](backgrounds/KbPsIOy2v6c7H87i.htm)|Political Scion|Rejeton de politicien|libre|
|[kH8lJq7NCoMlyOUj.htm](backgrounds/kH8lJq7NCoMlyOUj.htm)|Clown|Clown|libre|
|[KUcgqmbOnKiNdKru.htm](backgrounds/KUcgqmbOnKiNdKru.htm)|Grizzled Muckrucker|Fangeux grisonnant|libre|
|[l1yFuSDrHiylStW1.htm](backgrounds/l1yFuSDrHiylStW1.htm)|Sally Guard Neophyte|Néophyte de la Garde des Percées|libre|
|[lGMSYgWqZrg2SaTH.htm](backgrounds/lGMSYgWqZrg2SaTH.htm)|Fortune Teller|Voyant|officielle|
|[lMF9dnqUeCDED0TG.htm](backgrounds/lMF9dnqUeCDED0TG.htm)|Animal Wrangler|Soigneur animalier|libre|
|[LPZmM7UnWddi80gB.htm](backgrounds/LPZmM7UnWddi80gB.htm)|Mystic Seer|Voyant mystique|libre|
|[ltRrwn4OZcc5FC1G.htm](backgrounds/ltRrwn4OZcc5FC1G.htm)|Reputation Seeker|En quête de renommée|officielle|
|[LY8vRo3Pyn7FxWNj.htm](backgrounds/LY8vRo3Pyn7FxWNj.htm)|Prisoner|Prisonnier|officielle|
|[MgMERnqmXRZcXHLH.htm](backgrounds/MgMERnqmXRZcXHLH.htm)|Demon Slayer|Tueur de démons|libre|
|[mO0VwDSEZJUg3jhf.htm](backgrounds/mO0VwDSEZJUg3jhf.htm)|Insurgent|Insurgé|libre|
|[MWJHfS2bsYTvdroc.htm](backgrounds/MWJHfS2bsYTvdroc.htm)|Bekyar Restorer|Restaurateur Bekyar|libre|
|[NDhkBcdxy5J8M6Wx.htm](backgrounds/NDhkBcdxy5J8M6Wx.htm)|Taldan Schemer|Intrigant taldorien|libre|
|[NEM45HOKdrp1PiDX.htm](backgrounds/NEM45HOKdrp1PiDX.htm)|Oenopion Ooze-Tender|Soigneur de vase d'Œnopion|libre|
|[ngzWTz11vCUI3h7i.htm](backgrounds/ngzWTz11vCUI3h7i.htm)|Lesser Scion|Benjamin|libre|
|[nhsqxxBfkcOMZBTp.htm](backgrounds/nhsqxxBfkcOMZBTp.htm)|Shadow Haunted|Ombre hantée|libre|
|[NiuVJNmAe7TdI3mv.htm](backgrounds/NiuVJNmAe7TdI3mv.htm)|Thrune Loyalist|Loyaliste Thrune|libre|
|[nqyKrbSd2AdQxjNh.htm](backgrounds/nqyKrbSd2AdQxjNh.htm)|Witch Wary|Gare aux sorcières|libre|
|[O23kJuciAtakDENn.htm](backgrounds/O23kJuciAtakDENn.htm)|Mantis Scion|Fils de la Mante|libre|
|[O4BXvhZpMvpmQEUs.htm](backgrounds/O4BXvhZpMvpmQEUs.htm)|Nexian Mystic|Mystique Nexien|libre|
|[O6Cd575MzIqYbvOR.htm](backgrounds/O6Cd575MzIqYbvOR.htm)|Sarkorian Survivor|Survivant du sarkoris|libre|
|[oFFk7lsMwXgxgQhI.htm](backgrounds/oFFk7lsMwXgxgQhI.htm)|Hellknight Historian|Historien des chevaliers infernaux|officielle|
|[oH5Uagjkc58AHozy.htm](backgrounds/oH5Uagjkc58AHozy.htm)|Chelish Rebel|Rebelle chélaxien|libre|
|[oZC6fev9xw72O3vA.htm](backgrounds/oZC6fev9xw72O3vA.htm)|Sarkorian Reclaimer|Réclamateur du Sarkoris|libre|
|[pcuLoIrIjMos8k1z.htm](backgrounds/pcuLoIrIjMos8k1z.htm)|Barkeep|Tavernier|officielle|
|[PG0jDgRCfzuv8mrF.htm](backgrounds/PG0jDgRCfzuv8mrF.htm)|Kalistrade Follower|Suivant de Kalistrade|libre|
|[pi2azTcLFMq8IWo8.htm](backgrounds/pi2azTcLFMq8IWo8.htm)|Feral Child|Enfant sauvage|libre|
|[PKVDSZaBcflUL1Za.htm](backgrounds/PKVDSZaBcflUL1Za.htm)|Varisian Wanderer|Vagabond Varisien|libre|
|[PS2WiHDmJ9RWq0eJ.htm](backgrounds/PS2WiHDmJ9RWq0eJ.htm)|Bandit|Bandit|libre|
|[Psg1l8hEesyT3Wri.htm](backgrounds/Psg1l8hEesyT3Wri.htm)|Courier|Garçon de course|libre|
|[psz1WHMGUNFOPG37.htm](backgrounds/psz1WHMGUNFOPG37.htm)|Sodden Scavenger|récupérateur détrempé|libre|
|[pvqejr2ADrs9EoBM.htm](backgrounds/pvqejr2ADrs9EoBM.htm)|Molthuni Mercenary|Mercenaire du Molthune|libre|
|[q0hK6ih6h42fL4yD.htm](backgrounds/q0hK6ih6h42fL4yD.htm)|Shory Seeker|Chercheur Rivain|libre|
|[qHj5VrGgd0Gze9LL.htm](backgrounds/qHj5VrGgd0Gze9LL.htm)|Sleepless Suns Star|Étoile des soleils sans repos|libre|
|[qhY96No3S3ulObtQ.htm](backgrounds/qhY96No3S3ulObtQ.htm)|Goblinblood Orphan|Orphelin du sang gobelin|libre|
|[QjEpaKn5x9INfcfY.htm](backgrounds/QjEpaKn5x9INfcfY.htm)|Atteran Rancher|Rancher Atteran|libre|
|[QoE4PIIVn60tRxF4.htm](backgrounds/QoE4PIIVn60tRxF4.htm)|Street Urchin|Enfant des rues|officielle|
|[qxvr3Hbv9Olk1EZF.htm](backgrounds/qxvr3Hbv9Olk1EZF.htm)|Squire|Écuyer|libre|
|[R3z7yfqV0OyODRK8.htm](backgrounds/R3z7yfqV0OyODRK8.htm)|Criminal|Criminel|officielle|
|[R8tx1XfofmjUhCYG.htm](backgrounds/R8tx1XfofmjUhCYG.htm)|Local Scion|Enfant du pays|officielle|
|[Rc2l1j73EC2hPmUA.htm](backgrounds/Rc2l1j73EC2hPmUA.htm)|Refugee (Fall of Plaguestone)|Réfugié (la Chute de Plaguestone)|libre|
|[Rik6ZXovORQAfhPR.htm](backgrounds/Rik6ZXovORQAfhPR.htm)|Hunter|Chasseur|officielle|
|[rm9HYArb07JFDwjv.htm](backgrounds/rm9HYArb07JFDwjv.htm)|Scholar|Érudit|officielle|
|[RnH0Uo9vIJM68jZv.htm](backgrounds/RnH0Uo9vIJM68jZv.htm)|Wildwood Local|Habitué des bois|libre|
|[RsBbawZlgqkMh4Un.htm](backgrounds/RsBbawZlgqkMh4Un.htm)|Ulfen Raider|Raider ulfe|libre|
|[s3QsxcuSmGTjMTZN.htm](backgrounds/s3QsxcuSmGTjMTZN.htm)|Amnesiac|Amnésique|libre|
|[SbAaSeBHTmQ3xU8X.htm](backgrounds/SbAaSeBHTmQ3xU8X.htm)|Final Blade Survivor|Survivant de la Lame finale|libre|
|[sdPE9bha7IOb4O8i.htm](backgrounds/sdPE9bha7IOb4O8i.htm)|Butcher|"Boucher"|libre|
|[shwAcaWydDHBqR1X.htm](backgrounds/shwAcaWydDHBqR1X.htm)|Osirionologist|Osirionologiste|libre|
|[skqghxn3svID1dyU.htm](backgrounds/skqghxn3svID1dyU.htm)|Bright Lion|Lion brillant|libre|
|[SMqKFHj0Ut6j3NNo.htm](backgrounds/SMqKFHj0Ut6j3NNo.htm)|Haunting Vision|Hanté par une vision|officielle|
|[T6nf9vQKWzHui59P.htm](backgrounds/T6nf9vQKWzHui59P.htm)|Lost and Alone|Perdu et seul|libre|
|[Ta0rSeD79D62uXy2.htm](backgrounds/Ta0rSeD79D62uXy2.htm)|Godless Graycloack|Cape grise athée|libre|
|[TdEwgAZfOZ6fKGVM.htm](backgrounds/TdEwgAZfOZ6fKGVM.htm)|Martial Disciple|Disciple martial|officielle|
|[TDmwsOqt37L35nx6.htm](backgrounds/TDmwsOqt37L35nx6.htm)|Former Aspis Agent|Ancien Agent de l'Aspis|libre|
|[tPKeIokrWhzzf8jg.htm](backgrounds/tPKeIokrWhzzf8jg.htm)|Noble|Noble|changé|
|[Tqyybb0eKpwRHYwj.htm](backgrounds/Tqyybb0eKpwRHYwj.htm)|Magaambya Academic|Étudiant de Magaambya|libre|
|[tSzyLCQbf8T7l0xt.htm](backgrounds/tSzyLCQbf8T7l0xt.htm)|Hermit|Ermite|changé|
|[TtRmN9qYJnqbrfeV.htm](backgrounds/TtRmN9qYJnqbrfeV.htm)|Scholar of the Ancients|Étudiant de l'Antiquité|libre|
|[u13zJhBdawMYLGTk.htm](backgrounds/u13zJhBdawMYLGTk.htm)|Storm Survivor|Rescapé d'une tempête|libre|
|[U4VWN83sV1tT12Yf.htm](backgrounds/U4VWN83sV1tT12Yf.htm)|Merabite Prodigy|Prodige Merabite|libre|
|[UcQP3PBk7Ag7shxT.htm](backgrounds/UcQP3PBk7Ag7shxT.htm)|Thuvian Unifier|Unificateur de Thuvie|libre|
|[uDcJtxd6j47m1knn.htm](backgrounds/uDcJtxd6j47m1knn.htm)|Tax Collector|Percepteur|libre|
|[UDmScyDHUYMFbr7n.htm](backgrounds/UDmScyDHUYMFbr7n.htm)|Charlatan|Charlatan|officielle|
|[uEoo4fcZvXj1IC39.htm](backgrounds/uEoo4fcZvXj1IC39.htm)|Sailor|Marin|changé|
|[vBj8pruBbClwdEZG.htm](backgrounds/vBj8pruBbClwdEZG.htm)|Thassilonian Traveler|Voyageur Thassilonien|libre|
|[VJx0O3rGNjZ0X22p.htm](backgrounds/VJx0O3rGNjZ0X22p.htm)|Sewer Dragon|Dragon des égoûts|libre|
|[VnoaNi40DBN0ZV8y.htm](backgrounds/VnoaNi40DBN0ZV8y.htm)|Field Medic|Médecin militaire|officielle|
|[VOI91rOapoBqgUv7.htm](backgrounds/VOI91rOapoBqgUv7.htm)|Black Market Smuggler|Contrebandier du Marché noir|libre|
|[VoWlFBfksysrf8zY.htm](backgrounds/VoWlFBfksysrf8zY.htm)|Acolyte|Acolyte|changé|
|[Vu7pydxrARfYD67B.htm](backgrounds/Vu7pydxrARfYD67B.htm)|Ruby Phoenix Enthusiast|Enthousiaste du Phénix de rubis|libre|
|[Vuzq4NkLghj6dy2c.htm](backgrounds/Vuzq4NkLghj6dy2c.htm)|Detective|Détective|officielle|
|[vWVQYrsqSr1VZKkv.htm](backgrounds/vWVQYrsqSr1VZKkv.htm)|Cook|Cuisinier|libre|
|[VZzp5l7WX5v64XRk.htm](backgrounds/VZzp5l7WX5v64XRk.htm)|Blessed|Béni|libre|
|[w7kXY9PTVrF2YvTW.htm](backgrounds/w7kXY9PTVrF2YvTW.htm)|Undersea Enthusiast|Enthousiaste aquatique|libre|
|[WPyDbom83vu7Fy5i.htm](backgrounds/WPyDbom83vu7Fy5i.htm)|Droskari Disciple|Disciple de Droskar|libre|
|[WRbpRTHJesfbjIS5.htm](backgrounds/WRbpRTHJesfbjIS5.htm)|Shadow War Survivor|Survivant de la Guerre de l'ombre|libre|
|[XC86jXYUicUqkoQY.htm](backgrounds/XC86jXYUicUqkoQY.htm)|Mana Wastes Refugee|Réfugié de la Désolation de Mana|libre|
|[xKVVXEDgNeJAuIcX.htm](backgrounds/xKVVXEDgNeJAuIcX.htm)|Barker|Aboyeur|libre|
|[xLDNoWnNxtJceEwJ.htm](backgrounds/xLDNoWnNxtJceEwJ.htm)|Shadow Lodge Defector|Défecteur de la Loge de l'ombre|libre|
|[xlhQlrfhucYxkyPD.htm](backgrounds/xlhQlrfhucYxkyPD.htm)|Raised by Belief|Élevé dans la croyance|libre|
|[XnDdzNHhA17xRZ22.htm](backgrounds/XnDdzNHhA17xRZ22.htm)|Purveyor of the Bizzare|Fournisseur de Bizarreries|libre|
|[y6Df6HCrzc5kuLAJ.htm](backgrounds/y6Df6HCrzc5kuLAJ.htm)|Alkenstar Tinker|Bricoleur d'Alkenstar|libre|
|[Y6qKcSXLEyBfW3lS.htm](backgrounds/Y6qKcSXLEyBfW3lS.htm)|Farmhand|Ouvrier agricole|officielle|
|[y73tZ9iU3w84NrIG.htm](backgrounds/y73tZ9iU3w84NrIG.htm)|Dragon Scholar|Spécialiste des dragons|changé|
|[y9fqolddlRWRQsWl.htm](backgrounds/y9fqolddlRWRQsWl.htm)|Kyonin Emissary|Émissaire du Kyonin|libre|
|[ybCJZK6D4yY7AyVj.htm](backgrounds/ybCJZK6D4yY7AyVj.htm)|Royalty|De sang royal|libre|
|[yikXQI2YVWcmGV2N.htm](backgrounds/yikXQI2YVWcmGV2N.htm)|Acrobat|Acrobate|officielle|
|[yphbsrQe9KmQefU9.htm](backgrounds/yphbsrQe9KmQefU9.htm)|Emissary|Émissaire|officielle|
|[YS8l22rXhnLnyBqb.htm](backgrounds/YS8l22rXhnLnyBqb.htm)|Bonuwat Wavetouched|Bonuwat Touché par les vagues|libre|
|[YYGaSpcfC3XBnyg1.htm](backgrounds/YYGaSpcfC3XBnyg1.htm)|Vidrian Reformer|Réformateur vidrien|libre|
|[YYgcTGfA8UIJwv3Y.htm](backgrounds/YYgcTGfA8UIJwv3Y.htm)|Archaeologist|Archéologue|libre|
