# État de la traduction (classes)

 * **libre**: 4
 * **changé**: 12


Dernière mise à jour: 2020-11-16 19:02 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[03ga5t1V5GsvCGUi.htm](classes/03ga5t1V5GsvCGUi.htm)|Witch|Sorcière|libre|
|[6M4DO9lY25F8n6Li.htm](classes/6M4DO9lY25F8n6Li.htm)|Rogue|Roublard|changé|
|[8KGkzwIFYFHAdw2H.htm](classes/8KGkzwIFYFHAdw2H.htm)|Monk|Moine|changé|
|[8l1uexxIX7m6LUUv.htm](classes/8l1uexxIX7m6LUUv.htm)|Bard|Barde|changé|
|[Hp9n82T1VdG5fb9e.htm](classes/Hp9n82T1VdG5fb9e.htm)|Barbarian|Barbare|changé|
|[JSkS9iLsebH2HUSY.htm](classes/JSkS9iLsebH2HUSY.htm)|Druid|Druide|changé|
|[lifEnSfGjmeKcH8Y.htm](classes/lifEnSfGjmeKcH8Y.htm)|Swashbuckler|Bretteur|libre|
|[O6rpIOJQM78U9eyU.htm](classes/O6rpIOJQM78U9eyU.htm)|Wizard|Magicien|changé|
|[Oia1kqkF4KwdVVaw.htm](classes/Oia1kqkF4KwdVVaw.htm)|Cleric|Prêtre|changé|
|[p5zBqyyeNY1HTAV1.htm](classes/p5zBqyyeNY1HTAV1.htm)|Fighter|Guerrier|changé|
|[QzgMYQWYdQZMIChe.htm](classes/QzgMYQWYdQZMIChe.htm)|Champion|Champion|changé|
|[rieRSsMMpae94H7b.htm](classes/rieRSsMMpae94H7b.htm)|Alchemist|Alchimiste|changé|
|[tagRpjQsoYlO8Aj4.htm](classes/tagRpjQsoYlO8Aj4.htm)|Oracle|Oracle|libre|
|[WpMQGtgz46Cc5onF.htm](classes/WpMQGtgz46Cc5onF.htm)|Sorcerer|Ensorceleur|changé|
|[WreDTLuWC5Kqsglb.htm](classes/WreDTLuWC5Kqsglb.htm)|Investigator|Enquêteur|libre|
|[zk0K5exx506ukJns.htm](classes/zk0K5exx506ukJns.htm)|Ranger|Rôdeur|changé|
