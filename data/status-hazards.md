# État de la traduction (hazards)

 * **aucune**: 46


Dernière mise à jour: 2020-11-16 19:02 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[2GAOUxDfoA48uCWP.htm](hazards/2GAOUxDfoA48uCWP.htm)|Fireball Rune|
|[491qhVbjsHnOuMZW.htm](hazards/491qhVbjsHnOuMZW.htm)|Electric Latch Rune|
|[4jJxLlimyQaIVXlt.htm](hazards/4jJxLlimyQaIVXlt.htm)|Darkside Mirror|
|[4O7wKZdeAemTEbvG.htm](hazards/4O7wKZdeAemTEbvG.htm)|Slamming Door|
|[6In2S3lDnxNgZ2np.htm](hazards/6In2S3lDnxNgZ2np.htm)|Titanic Flytrap|
|[7VqibTAEXXX6PIhh.htm](hazards/7VqibTAEXXX6PIhh.htm)|Scythe Blades|
|[8ewUvJlvn6LVjoXJ.htm](hazards/8ewUvJlvn6LVjoXJ.htm)|Second Chance|
|[8gAoSgBJN8QqzP1R.htm](hazards/8gAoSgBJN8QqzP1R.htm)|Frozen Moment|
|[98rS64gLzy1ReXoR.htm](hazards/98rS64gLzy1ReXoR.htm)|Hampering Web|
|[A93flWUsot3FmC7t.htm](hazards/A93flWUsot3FmC7t.htm)|Yellow Mold|
|[AM3YY2Zfe2ChJHd7.htm](hazards/AM3YY2Zfe2ChJHd7.htm)|Telekinetic Swarm Trap|
|[BHq5wpQU8hQEke8D.htm](hazards/BHq5wpQU8hQEke8D.htm)|Hidden Pit|
|[BsZ6o2YrwVovKfNh.htm](hazards/BsZ6o2YrwVovKfNh.htm)|Armageddon Orb|
|[C6nFe8SCWJ8FmLOT.htm](hazards/C6nFe8SCWJ8FmLOT.htm)|Quicksand|
|[d3YklujLpBFC5HfB.htm](hazards/d3YklujLpBFC5HfB.htm)|Ghostly Choir|
|[g9HovYB4pfHgIML9.htm](hazards/g9HovYB4pfHgIML9.htm)|Flensing Blades|
|[gB9WkJtH88jJQa5Z.htm](hazards/gB9WkJtH88jJQa5Z.htm)|Plummeting Doom|
|[gFt2nzQrVgXM9tmJ.htm](hazards/gFt2nzQrVgXM9tmJ.htm)|Treacherous Scree|
|[H2GX04CQXLPQHT8h.htm](hazards/H2GX04CQXLPQHT8h.htm)|Wheel Of Misery|
|[H8CPGJn81JSTCRNx.htm](hazards/H8CPGJn81JSTCRNx.htm)|Polymorph Trap|
|[HnPd9Vqh5NHKEdRq.htm](hazards/HnPd9Vqh5NHKEdRq.htm)|Spinning Blade Pillar|
|[inUlZWE1isqnTRc5.htm](hazards/inUlZWE1isqnTRc5.htm)|Jealous Abjurer|
|[J4YChuob7MIPT5Mq.htm](hazards/J4YChuob7MIPT5Mq.htm)|Vorpal Executioner|
|[lgyGhpyPosriQUzE.htm](hazards/lgyGhpyPosriQUzE.htm)|Perilous Flash Flood|
|[LLPsEKLoVmoPleJS.htm](hazards/LLPsEKLoVmoPleJS.htm)|Green Slime|
|[lVqVDjXnHboMif7F.htm](hazards/lVqVDjXnHboMif7F.htm)|Poisoned Dart Gallery|
|[m4PRYxFq9ojcwesh.htm](hazards/m4PRYxFq9ojcwesh.htm)|Pharaoh's Ward|
|[mMXHyWdmmAN0GPvG.htm](hazards/mMXHyWdmmAN0GPvG.htm)|Banshee's Symphony|
|[nO4osrBRnpWKFCMP.htm](hazards/nO4osrBRnpWKFCMP.htm)|Summoning Rune|
|[O0qA1ElCOgYGEBtL.htm](hazards/O0qA1ElCOgYGEBtL.htm)|Eternal Flame|
|[OekigjNLNp9XENjx.htm](hazards/OekigjNLNp9XENjx.htm)|Drowning Pit|
|[oNLgR1iq6MVvNRWo.htm](hazards/oNLgR1iq6MVvNRWo.htm)|Lava Flume Tube|
|[Q8bXKgDm8eguqThB.htm](hazards/Q8bXKgDm8eguqThB.htm)|Brown Mold|
|[sFaAWmy1szDRmFtk.htm](hazards/sFaAWmy1szDRmFtk.htm)|Confounding Betrayal|
|[siylw0zIh1g4VnCW.htm](hazards/siylw0zIh1g4VnCW.htm)|Grasp Of The Damned|
|[su4TRBOoso4vjkoK.htm](hazards/su4TRBOoso4vjkoK.htm)|Bloodthirsty Urge|
|[tbwGr6FIr5WpvQ6l.htm](hazards/tbwGr6FIr5WpvQ6l.htm)|Hammer Of Forbiddance|
|[uEZ4Jv2wNyukJTRL.htm](hazards/uEZ4Jv2wNyukJTRL.htm)|Hallucination Powder Trap|
|[Uw2iVgbbeyn3mOjt.htm](hazards/Uw2iVgbbeyn3mOjt.htm)|Spectral Reflection|
|[v2xIxZ9ZZ6fJyATF.htm](hazards/v2xIxZ9ZZ6fJyATF.htm)|Poisoned Lock|
|[VA4VL3kVUxBYbwRf.htm](hazards/VA4VL3kVUxBYbwRf.htm)|Snowfall|
|[vlMuFskctUvjJe8X.htm](hazards/vlMuFskctUvjJe8X.htm)|Spear Launcher|
|[vTdWEBzJzltMM6r4.htm](hazards/vTdWEBzJzltMM6r4.htm)|Shrieker|
|[xkqjwu1ox0pQLOnb.htm](hazards/xkqjwu1ox0pQLOnb.htm)|Bottomless Pit|
|[yM4G2LvMwvkIRx0G.htm](hazards/yM4G2LvMwvkIRx0G.htm)|Planar Rift|
|[ZUCGvc2dTJUlM9dC.htm](hazards/ZUCGvc2dTJUlM9dC.htm)|Dance Of Death|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
