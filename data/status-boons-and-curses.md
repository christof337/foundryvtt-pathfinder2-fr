# État de la traduction (boons-and-curses)

 * **libre**: 26
 * **aucune**: 94


Dernière mise à jour: 2020-11-16 19:02 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[a1OZtMQSzjYOm0P3.htm](boons-and-curses/a1OZtMQSzjYOm0P3.htm)|Rovagug - Moderate Boon|
|[aOVXhHVkLw9dLCdR.htm](boons-and-curses/aOVXhHVkLw9dLCdR.htm)|Zon-Kuthon - Moderate Curse|
|[aV1I9VmMQBaFU9M9.htm](boons-and-curses/aV1I9VmMQBaFU9M9.htm)|Asmodeus - Moderate Boon|
|[c2wwILmpgyXIjlfe.htm](boons-and-curses/c2wwILmpgyXIjlfe.htm)|Torag - Major Curse|
|[cFz09CWKzff2cC73.htm](boons-and-curses/cFz09CWKzff2cC73.htm)|Rovagug - Major Boon|
|[d198p8UBPK3VcKi9.htm](boons-and-curses/d198p8UBPK3VcKi9.htm)|Rovagug - Moderate Curse|
|[DfXlr5qqbcjJN3gh.htm](boons-and-curses/DfXlr5qqbcjJN3gh.htm)|Abadar - Moderate Boon|
|[dHcGihuimtCf1NOs.htm](boons-and-curses/dHcGihuimtCf1NOs.htm)|Iomedae - Major Boon|
|[dL6r0VfFIWlAazDW.htm](boons-and-curses/dL6r0VfFIWlAazDW.htm)|Saranrae - Major Boon|
|[dLudHCFcj4p7KG3j.htm](boons-and-curses/dLudHCFcj4p7KG3j.htm)|Asmodeus - Minor Curse|
|[DnlAmlrrKv3jwaOq.htm](boons-and-curses/DnlAmlrrKv3jwaOq.htm)|Pharasma - Major Curse|
|[dTJP2Hg4ZJu4Ck2y.htm](boons-and-curses/dTJP2Hg4ZJu4Ck2y.htm)|Iomedae - Minor Boon|
|[DuGA57MDiwEKp7Y1.htm](boons-and-curses/DuGA57MDiwEKp7Y1.htm)|Saranrae - Major Curse|
|[dZU3HdI2oO8LFjGq.htm](boons-and-curses/dZU3HdI2oO8LFjGq.htm)|Urgathoa - Major Boon|
|[e7Y4jKHQ3ptbSuxc.htm](boons-and-curses/e7Y4jKHQ3ptbSuxc.htm)|Torag - Minor Boon|
|[e9bdt8UH7ZvpRrLz.htm](boons-and-curses/e9bdt8UH7ZvpRrLz.htm)|Saranrae - Minor Boon|
|[eibN2Uf0dsHCU5rE.htm](boons-and-curses/eibN2Uf0dsHCU5rE.htm)|Zon-Kuthon - Major Boon|
|[eVRjDKt0N0qSmpCn.htm](boons-and-curses/eVRjDKt0N0qSmpCn.htm)|Urgathoa - Major Curse|
|[FeCcooyIb1JDQhd7.htm](boons-and-curses/FeCcooyIb1JDQhd7.htm)|Calistra - Major Boon|
|[fLBNXULLPalAKlYe.htm](boons-and-curses/fLBNXULLPalAKlYe.htm)|Cayden Cailean - Minor Curse|
|[fsKVskUhWwMgvUaT.htm](boons-and-curses/fsKVskUhWwMgvUaT.htm)|Cayden Cailean - Moderate Boon|
|[fV0t8xagcbqwvTpQ.htm](boons-and-curses/fV0t8xagcbqwvTpQ.htm)|Asmodeus - Minor Boon|
|[fV0Xa1Bd3BoWACgT.htm](boons-and-curses/fV0Xa1Bd3BoWACgT.htm)|Irori - Minor Boon|
|[fV940VM5RCsNwUvA.htm](boons-and-curses/fV940VM5RCsNwUvA.htm)|Calistra - Major Curse|
|[gDBa72Y2jokr8Zzg.htm](boons-and-curses/gDBa72Y2jokr8Zzg.htm)|Lamashtu - Moderate Curse|
|[HdPVL76QiDhsLCA9.htm](boons-and-curses/HdPVL76QiDhsLCA9.htm)|Nethys - Moderate Boon|
|[hrTl9kfSNrOQeNze.htm](boons-and-curses/hrTl9kfSNrOQeNze.htm)|Erastil - Minor Boon|
|[HrV31rAkNjV4KfCU.htm](boons-and-curses/HrV31rAkNjV4KfCU.htm)|Gorum - Minor Boon|
|[hUSknn3LYmT7eKzT.htm](boons-and-curses/hUSknn3LYmT7eKzT.htm)|Norgorber - Moderate Boon|
|[ILtU0TAUZcOcwkkG.htm](boons-and-curses/ILtU0TAUZcOcwkkG.htm)|Shelyn - Major Curse|
|[IOaepssdwTanFooc.htm](boons-and-curses/IOaepssdwTanFooc.htm)|Iomedae - Minor Curse|
|[jfWeNqYj3rn9EysM.htm](boons-and-curses/jfWeNqYj3rn9EysM.htm)|Urgathoa - Minor Boon|
|[jhixjUJZlCetNRjH.htm](boons-and-curses/jhixjUJZlCetNRjH.htm)|Shelyn - Minor Curse|
|[jVeuBZsa4iFg0wC6.htm](boons-and-curses/jVeuBZsa4iFg0wC6.htm)|Nethys - Minor Boon|
|[kCCrjskDIbG0e74O.htm](boons-and-curses/kCCrjskDIbG0e74O.htm)|Irori - Major Boon|
|[kEAw5PMLdX2gHlwR.htm](boons-and-curses/kEAw5PMLdX2gHlwR.htm)|Gozreh - Major Curse|
|[KjPTFfxMAqCngQFB.htm](boons-and-curses/KjPTFfxMAqCngQFB.htm)|Nethys - Minor Curse|
|[Ks1HaYEuSRybaq8U.htm](boons-and-curses/Ks1HaYEuSRybaq8U.htm)|Calistra - Moderate Boon|
|[kstqMWge32n0Lfdz.htm](boons-and-curses/kstqMWge32n0Lfdz.htm)|Gorum - Moderate Boon|
|[l7n8n4fAoRLIn95W.htm](boons-and-curses/l7n8n4fAoRLIn95W.htm)|Shelyn - Major Boon|
|[La6t4fVId26PkmYk.htm](boons-and-curses/La6t4fVId26PkmYk.htm)|Cayden Cailean - Minor Boon|
|[lI7bDExxA2zQq1sP.htm](boons-and-curses/lI7bDExxA2zQq1sP.htm)|Zon-Kuthon - Minor Curse|
|[lOIu9jYDAR2rDe4p.htm](boons-and-curses/lOIu9jYDAR2rDe4p.htm)|Norgorber - Major Boon|
|[LOlv11WEGUQWYKna.htm](boons-and-curses/LOlv11WEGUQWYKna.htm)|Desna - Minor Boon|
|[lR9KS3pQ4gGKNspP.htm](boons-and-curses/lR9KS3pQ4gGKNspP.htm)|Gorum - Moderate Curse|
|[m33aOQij1BDcArN9.htm](boons-and-curses/m33aOQij1BDcArN9.htm)|Abadar - Major Boon|
|[M78A7uQaa4Ig8pGU.htm](boons-and-curses/M78A7uQaa4Ig8pGU.htm)|Gorum - Major Curse|
|[mcVlGufQ8rQRe61Y.htm](boons-and-curses/mcVlGufQ8rQRe61Y.htm)|Desna - Major Boon|
|[N29LCwYfRWHtEqrS.htm](boons-and-curses/N29LCwYfRWHtEqrS.htm)|Urgathoa - Moderate Boon|
|[n5p8QfZdptRZp0ii.htm](boons-and-curses/n5p8QfZdptRZp0ii.htm)|Abadar - Moderate Curse|
|[NeAA1BHGGkRAmBDe.htm](boons-and-curses/NeAA1BHGGkRAmBDe.htm)|Abadar - Minor Curse|
|[nFrCY6tT2B8uxaO3.htm](boons-and-curses/nFrCY6tT2B8uxaO3.htm)|Erastil - Major Boon|
|[NgwW7Wt2gO9SEwPX.htm](boons-and-curses/NgwW7Wt2gO9SEwPX.htm)|Gorum - Minor Curse|
|[nqOoUH4r8NEi1i2g.htm](boons-and-curses/nqOoUH4r8NEi1i2g.htm)|Calistra - Moderate Curse|
|[OaUt41v2OrQkHpM4.htm](boons-and-curses/OaUt41v2OrQkHpM4.htm)|Abadar - Major Curse|
|[ParIWsb6B4fdhrHF.htm](boons-and-curses/ParIWsb6B4fdhrHF.htm)|Pharasma - Minor Curse|
|[PoHos7qriDzQN7Gw.htm](boons-and-curses/PoHos7qriDzQN7Gw.htm)|Rovagug - Major Curse|
|[pwmFrNkLscXrDSPN.htm](boons-and-curses/pwmFrNkLscXrDSPN.htm)|Erastil - Minor Curse|
|[py9v5HfCIzCoyC0C.htm](boons-and-curses/py9v5HfCIzCoyC0C.htm)|Lamashtu - Major Curse|
|[QGUFWuz9uF49EIhy.htm](boons-and-curses/QGUFWuz9uF49EIhy.htm)|Cayden Cailean - Major Curse|
|[qhVfbZCxZJS8NmB3.htm](boons-and-curses/qhVfbZCxZJS8NmB3.htm)|Irori - Moderate Curse|
|[qTmjEC528YnvPgXE.htm](boons-and-curses/qTmjEC528YnvPgXE.htm)|Iomedae - Moderate Boon|
|[QVEkEdxCnoGxMo9l.htm](boons-and-curses/QVEkEdxCnoGxMo9l.htm)|Zon-Kuthon - Moderate Boon|
|[rA7ZM6WGnJNrWTmo.htm](boons-and-curses/rA7ZM6WGnJNrWTmo.htm)|Iomedae - Moderate Curse|
|[S1qGyGG25YImRhmS.htm](boons-and-curses/S1qGyGG25YImRhmS.htm)|Saranrae - Moderate Boon|
|[SaiJX9KBN3lC0RUy.htm](boons-and-curses/SaiJX9KBN3lC0RUy.htm)|Zon-Kuthon - Major Curse|
|[sy5UyOvxitqX244D.htm](boons-and-curses/sy5UyOvxitqX244D.htm)|Asmodeus - Major Boon|
|[tFnPBYcAZ0X3GbI5.htm](boons-and-curses/tFnPBYcAZ0X3GbI5.htm)|Lamashtu - Moderate Boon|
|[tilqIbJZLqeknTYo.htm](boons-and-curses/tilqIbJZLqeknTYo.htm)|Shelyn - Minor Boon|
|[U7ZJfuPLQPyoaj4M.htm](boons-and-curses/U7ZJfuPLQPyoaj4M.htm)|Pharasma - Major Boon|
|[Up7nUIT42zwhgZf4.htm](boons-and-curses/Up7nUIT42zwhgZf4.htm)|Desna - Minor Curse|
|[uVZrqxtOOutr4Ss9.htm](boons-and-curses/uVZrqxtOOutr4Ss9.htm)|Gozreh - Minor Boon|
|[V7FYIgphCCkYsEXF.htm](boons-and-curses/V7FYIgphCCkYsEXF.htm)|Saranrae - Moderate Curse|
|[vfQ7TsScMBTNiznn.htm](boons-and-curses/vfQ7TsScMBTNiznn.htm)|Gozreh - Major Boon|
|[vU5leGDNAZSfPgTz.htm](boons-and-curses/vU5leGDNAZSfPgTz.htm)|Shelyn - Moderate Curse|
|[VZodqtMF3qrMl8a7.htm](boons-and-curses/VZodqtMF3qrMl8a7.htm)|Saranrae - Minor Curse|
|[wEqPbkQMuqlL2TPn.htm](boons-and-curses/wEqPbkQMuqlL2TPn.htm)|Irori - Major Curse|
|[wIjqVRP0Hm2bIfyc.htm](boons-and-curses/wIjqVRP0Hm2bIfyc.htm)|Calistra - Minor Curse|
|[wlBiDpWvjKiw9k2z.htm](boons-and-curses/wlBiDpWvjKiw9k2z.htm)|Zon-Kuthon - Minor Boon|
|[wREsKC8CTifEyj6v.htm](boons-and-curses/wREsKC8CTifEyj6v.htm)|Norgorber - Major Curse|
|[x1QrKuvZ2TbTvLUY.htm](boons-and-curses/x1QrKuvZ2TbTvLUY.htm)|Cayden Cailean - Moderate Curse|
|[XFd7rnb9cchNw0WN.htm](boons-and-curses/XFd7rnb9cchNw0WN.htm)|Norgorber - Minor Curse|
|[XFiI9qC4MNX4WUdh.htm](boons-and-curses/XFiI9qC4MNX4WUdh.htm)|Lamashtu - Major Boon|
|[XfMZ83VA7dtF7fL5.htm](boons-and-curses/XfMZ83VA7dtF7fL5.htm)|Urgathoa - Minor Curse|
|[xrczhivVmi7MrEfz.htm](boons-and-curses/xrczhivVmi7MrEfz.htm)|Irori - Moderate Boon|
|[xrSgGobm5QtTcABQ.htm](boons-and-curses/xrSgGobm5QtTcABQ.htm)|Pharasma - Moderate Boon|
|[XYM7v72doPR3LwgA.htm](boons-and-curses/XYM7v72doPR3LwgA.htm)|Gozreh - Minor Curse|
|[y9DNpX7krUiOq0YI.htm](boons-and-curses/y9DNpX7krUiOq0YI.htm)|Asmodeus - Moderate Curse|
|[yoXU4CqZtzgKXdlB.htm](boons-and-curses/yoXU4CqZtzgKXdlB.htm)|Asmodeus - Major Curse|
|[yWASKzumwmv4TgSe.htm](boons-and-curses/yWASKzumwmv4TgSe.htm)|Calistra - Minor Boon|
|[z4GfG3GatAOELKyA.htm](boons-and-curses/z4GfG3GatAOELKyA.htm)|Desna - Major Curse|
|[zoJjzir97Ie7tbp0.htm](boons-and-curses/zoJjzir97Ie7tbp0.htm)|Shelyn - Moderate Boon|
|[ZPlvqkjEv9a7J76T.htm](boons-and-curses/ZPlvqkjEv9a7J76T.htm)|Cayden Cailean - Major Boon|
|[zWshZRICuzP7DfFV.htm](boons-and-curses/zWshZRICuzP7DfFV.htm)|Iomedae - Major Curse|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0PMqydlsIjj8GNnl.htm](boons-and-curses/0PMqydlsIjj8GNnl.htm)|Norgorber - Moderate Curse|Norgorber - Malédiction modérée|libre|
|[0vmwZrIhm5rWHkYh.htm](boons-and-curses/0vmwZrIhm5rWHkYh.htm)|Pharasma - Minor Boon|Pharasma - Bénédiction mineure|libre|
|[1hdWNyedYvuZgtPr.htm](boons-and-curses/1hdWNyedYvuZgtPr.htm)|Pharasma - Moderate Curse|Pharasma - Malédiction modérée|libre|
|[1nALC8yWtjBDEaOC.htm](boons-and-curses/1nALC8yWtjBDEaOC.htm)|Rovagug - Minor Curse|Rovagug - Malédiction mineure|libre|
|[2EKgNifq3ozzKYfI.htm](boons-and-curses/2EKgNifq3ozzKYfI.htm)|Norgorber - Minor Boon|Norgorber - Bénédiction mineure|libre|
|[30Xdyvplx7MfX1nA.htm](boons-and-curses/30Xdyvplx7MfX1nA.htm)|Torag - Moderate Boon|Torag - Bénédiction modérée|libre|
|[3u3vav6qfAW6hPKE.htm](boons-and-curses/3u3vav6qfAW6hPKE.htm)|Erastil - Moderate Curse|Érastil - Malédiction modérée|libre|
|[4FkVB4uQK4eHZJ6Z.htm](boons-and-curses/4FkVB4uQK4eHZJ6Z.htm)|Rovagug - Minor Boon|Rovagug - Bénédiction mineure|libre|
|[4m2dQ2fvVWxtokVe.htm](boons-and-curses/4m2dQ2fvVWxtokVe.htm)|Desna - Moderate Boon|Desna - Bénédiction modérée|libre|
|[4sZ9tpmPj4LIgPvU.htm](boons-and-curses/4sZ9tpmPj4LIgPvU.htm)|Nethys - Major Curse|Néthys - Malédiction majeure|libre|
|[5x0wEbgxYmXonBuN.htm](boons-and-curses/5x0wEbgxYmXonBuN.htm)|Lamashtu - Minor Curse|Lamashtu - Malédiction mineure|libre|
|[6f8zTNIs5XXzkhkR.htm](boons-and-curses/6f8zTNIs5XXzkhkR.htm)|Lamashtu - Minor Boon|Lamashtu - Bénédiction mineure|libre|
|[845vvISfgkk6sei0.htm](boons-and-curses/845vvISfgkk6sei0.htm)|Torag - Minor Curse|Torag - Malédiction mineure|libre|
|[8G0USrxM7d4B4EVI.htm](boons-and-curses/8G0USrxM7d4B4EVI.htm)|Gorum - Major Boon|Gorum - Bénédiction majeure|libre|
|[8ZNtiMxoJIP1DJ9Q.htm](boons-and-curses/8ZNtiMxoJIP1DJ9Q.htm)|Nethys - Major Boon|Néthys - Bénédiction majeure|libre|
|[9EMaYf6odFEyjdSr.htm](boons-and-curses/9EMaYf6odFEyjdSr.htm)|Urgathoa - Moderate Curse|Urgathoa - Malédiction Modérée|libre|
|[AdNAeWhnq5Is3AZb.htm](boons-and-curses/AdNAeWhnq5Is3AZb.htm)|Torag - Major Boon|Torag - Bénédiction majeure|libre|
|[AfuH02H6ib4KYS6C.htm](boons-and-curses/AfuH02H6ib4KYS6C.htm)|Torag - Moderate Curse|Torag - Malédiction modérée|libre|
|[AohWtOzgk2Qr9ADl.htm](boons-and-curses/AohWtOzgk2Qr9ADl.htm)|Gozreh - Moderate Curse|Gozreh - Malédiction modérée|libre|
|[AvjRCVzLmxapFOLV.htm](boons-and-curses/AvjRCVzLmxapFOLV.htm)|Gozreh - Moderate Boon|Gozreh - Bénédiction modérée|libre|
|[C6Ercv1ugcTsbVA9.htm](boons-and-curses/C6Ercv1ugcTsbVA9.htm)|Erastil - Major Curse|Érastil - Malédiction majeure|libre|
|[CjRUOscsOwCmai8D.htm](boons-and-curses/CjRUOscsOwCmai8D.htm)|Desna - Moderate Curse|Desna - Malédiction modérée|libre|
|[CL8lLQJWd4N89QIm.htm](boons-and-curses/CL8lLQJWd4N89QIm.htm)|Erastil - Moderate Boon|Érastil - Bénédiction modérée|libre|
|[CmHQGVyNZ7aOmMcd.htm](boons-and-curses/CmHQGVyNZ7aOmMcd.htm)|Nethys - Moderate Curse|Néthys - Malédiction modérée|libre|
|[DSfHCm8PDL5hLzZ0.htm](boons-and-curses/DSfHCm8PDL5hLzZ0.htm)|Irori - Minor Curse|Irori - Malédiction mineure|libre|
|[fXEeTSVINdWaKHhw.htm](boons-and-curses/fXEeTSVINdWaKHhw.htm)|Abadar - Minor Boon|Abadar - Bénédiction mineure|libre|
